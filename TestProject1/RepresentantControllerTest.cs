﻿using System.Collections.Generic;
using ElectionCandidates.Classes;
using ElectionCandidates.Controllers;
using ElectionCandidates.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Web.Mvc;
using System.Configuration;
namespace TestProject1
{


    /// <summary>
    ///This is a test class for RepresentantControllerTest and is intended
    ///to contain all RepresentantControllerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RepresentantControllerTest
    {


        private TestContext _testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return _testContextInstance; }
            set { _testContextInstance = value; }
        }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion


        /// <summary>
        ///A test for Index
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Visual Studio Projects\\Websites\\NTBValgRepresentanter\\ValgRepresentanter",
            "/")]
        [UrlToTest("http://localhost:60783/")]
        public void IndexTest()
        {
            CandidateController controller = new CandidateController();

            CandidatesModels repModels = new CandidatesModels();

            List<Candidate> representanter = repModels.GetRepresentanter(2011);

            Assert.IsNotNull(representanter);


        }
    }
}
