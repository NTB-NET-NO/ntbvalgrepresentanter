﻿using ElectionCandidates.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace TestProject1
{
    
    
    /// <summary>
    ///This is a test class for FileTest and is intended
    ///to contain all FileTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FileTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for CreateFilename
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Visual Studio Projects\\Websites\\NTBValgRepresentanter\\ValgRepresentanter", "/")]
        [UrlToTest("http://localhost:60783/")]
        [DeploymentItem("ElectionCandidates.dll")]
        public void CreateFilenameTest()
        {
            string fullName = "Trond Husø"; // TODO: Initialize to an appropriate value
            string politicalPartyCode = "Ap"; // TODO: Initialize to an appropriate value
            string county = "3"; // TODO: Initialize to an appropriate value
            string expected = "Ap_03_TrondHusoe"; // TODO: Initialize to an appropriate value
            string actual;
            actual = File_Accessor.CreateFilename(fullName, politicalPartyCode, county);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for CreateDirectoryName
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Visual Studio Projects\\Websites\\NTBValgRepresentanter\\ValgRepresentanter", "/")]
        [UrlToTest("http://localhost:60783/")]
        [DeploymentItem("ElectionCandidates.dll")]
        public void CreateDirectoryNameTest()
        {
            File_Accessor target = new File_Accessor(); // TODO: Initialize to an appropriate value
            string politicalParty = "Ap"; // TODO: Initialize to an appropriate value
            string county = "3"; // TODO: Initialize to an appropriate value
            string expected = @"\Ap\03\"; // TODO: Initialize to an appropriate value
            string actual = target.CreateDirectoryName(politicalParty, county);
            Assert.AreEqual(expected, actual);
        }
    }
}
