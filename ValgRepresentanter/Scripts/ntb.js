﻿$(document).ready(function () {
    $(".editrow").hide();
    $(".addrow").hide();

    $("#addNomination").bind("click", function () {
        $(".addrow").show('slow');
        return false;
    });

    $(".deleteCandidate").live("click", function () {
        var object = $(this);
        var link = object.attr("href");

        $.ajax({
            type: "GET",
            url: link,
            dataType: "text",
            cache: false,
            success: function (request) {
                $("#candidateList").html(request);

                $(".editrow").hide();
                $('.formlist').hide();
                $('.addrow').hide();
                // alert(html(request));

                return false;

            },
            error: function (xhr, ajaxOptions, error) {

                alert(xhr.status);
                alert(error);

            }
        });
        return false;
    });

    $(".ChangeElectionData").live("submit", function (e) {
        var object = $(this);
        var formClass = object.attr("class");
        // var electionId = object.attr("rel");
        var electionId = object.attr("id").replace(formClass, "");
        var link = object.attr("action");

        e.preventDefault();


        // Now we shall do ajax again
        $.ajax({
            type: "POST",
            url: link,
            data: object.serialize(),
            dataType: "text",
            cache: false,
            success: function (request) {
                
                $("#candidateList").html(request);
                $('.formlist').hide();
                $('.editrow').hide();
                $('.addrow').hide();

                return false;
            },
            error: function (xhr, ajaxOptions, error) {
                alert(xhr.status);
                alert(error);
            }

        });



        return false;
    });

    $(".editCandidate").live("click", function (e) {
        var object = $(this);


        e.preventDefault();

        var thisid = "";
        thisid = object.attr("rel");

        $("#" + thisid).show();

        return false;
    });

    $(".AddElectionData").live("submit", function (e) {
        var object = $(this);
        var formClass = object.attr("class");
        var electionId = object.attr("id").replace(formClass, "");
        var link = object.attr("action");

        e.preventDefault();


        // Now we shall do ajax again
        $.ajax({
            type: "POST",
            url: link,
            data: $(this).serialize(),
            dataType: "text",
            cache: false,
            success: function (request) {

                $("#candidateList").html(request);
                $(".editrow").hide();
                $('.formlist').hide();
                $('.addrow').hide();

                return false;
            },
            error: function (xhr, ajaxOptions, error) {
                alert(xhr.status);
                alert(error);

            }
        });
        return false;
    });




    $("input[type=reset]").click(function () {
        $(".editrow").hide();
        return false;
    });

    $("a.CandidateListe").click(function () {
        var $link = $(this);
        var thisid = "";
        thisid = this.rel;

        if (thisid != "") {
            $("#" + thisid).dialog({
                resizeable: false,
                height: 200,
                modal: true,
                buttons: {
                    'Delete': function () {
                        location: href = $link.attr('href');
                        $(this).dialog('close');
                    },
                    Cancel: function () {
                        $(this).dialog('close');
                    }
                }
            });
            return false;
        }
        return true;
    });

    $("#PartiFilter").change(function () {
        var partiCode = this.value;
        // window.location.href = '/Candidate/List/' + partiCode;

        // Checking if the thing has a value
        if (partiCode == '') {
            window.location.href = '/Candidate';
        } else {
            window.location.href = '/Candidate/List/' + partiCode;
        }
        //        if (partiCode != '') {
        //            $.ajax({
        //                type: 'GET',
        //                data: { PartiCode: partiCode },
        //                url: '/Candidate/List/',
        //                success: function (result) {
        //                    alert(result.name);
        //                },
        //                error: function () {
        //                    alert("Error");
        //                }
        //            });
        //        }
    });

});