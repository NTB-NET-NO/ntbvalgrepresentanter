﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SocialConnectionModel.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the SocialConnectionModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Models
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using ElectionCandidates.Classes;

    /// <summary>
    /// The social connection model.
    /// </summary>
    public class SocialConnectionModel
    {
        /// <summary>
        /// The is initialized.
        /// </summary>
        public bool IsInitialized;

        /// <summary>
        /// Initializes a new instance of the <see cref="SocialConnectionModel"/> class.
        /// </summary>
        public SocialConnectionModel()
        {
            IsInitialized = true;
        }

        /// <summary>
        /// The get social connections by candidate id.
        /// </summary>
        /// <param name="candidateId">
        /// The candidate id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<SocialConnection> GetSocialConnectionsByCandidateId(int candidateId)
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetSocialConnections", sqlConnection);
                sqlCommand.Parameters.Add(
                    new SqlParameter("@SocialConnectionId", DBNull.Value));
                sqlCommand.Parameters.Add(
                    new SqlParameter("@RepresentantId", candidateId));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                List<SocialConnection> socialConnections = new List<SocialConnection>();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            int socialConnectionId = 0;
                            
                            // Checking if the values from the database is not Null
                            if (sqlDataReader["SocialConnectionId"] != DBNull.Value)
                            {
                                socialConnectionId = Convert.ToInt32(sqlDataReader["SocialConnectionId"]);
                            }

                            // populating string with data from database
                            string userName = sqlDataReader["UserName"].ToString();
                            
                            string formFieldName = sqlDataReader["FormFieldName"].ToString();

                            string socialConnectionName = sqlDataReader["Name"].ToString();

                            // Creating a Social Connection object
                            SocialConnection socialConnection = new SocialConnection
                            {
                                Id = socialConnectionId,
                                Name = socialConnectionName,
                                UserName = userName,
                                FormFieldName = formFieldName
                            };

                            // Adding the social connection object to the list
                            socialConnections.Add(socialConnection);
                        }
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();

                return socialConnections;
            }
        }

        /// <summary>
        /// The get social connections.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<SocialConnection> GetSocialConnections()
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetSocialConnections", sqlConnection);

                // Adding parameter NULL to the SQL-Command
                sqlCommand.Parameters.Add(
                    new SqlParameter("@SocialConnectionId", DBNull.Value));

                // Adding parameter NULL to the SQL-Command
                sqlCommand.Parameters.Add(
                    new SqlParameter("@RepresentantId", DBNull.Value));
                
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                List<SocialConnection> socialConnections = new List<SocialConnection>();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            int socialConnectionId = 0;
                            string userName = string.Empty;

                            // Checking if the values from the database is not Null
                            if (sqlDataReader["SocialConnectionId"] != DBNull.Value)
                            {
                                socialConnectionId = Convert.ToInt32(sqlDataReader["SocialConnectionId"]);
                            }

                            string formFieldName = sqlDataReader["FormFieldName"].ToString();

                            string socialConnectionName = sqlDataReader["Name"].ToString();

                            SocialConnection socialConnection = new SocialConnection
                                {
                                    Id = socialConnectionId,
                                    Name = socialConnectionName,
                                    FormFieldName = formFieldName,
                                    UserName = userName
                                };

                            socialConnections.Add(socialConnection);
                        }
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();

                return socialConnections;
            }
        }
    }
}