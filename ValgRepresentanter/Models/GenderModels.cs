﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GenderModels.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the GenderModels type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Models
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using ElectionCandidates.Classes;

    /// <summary>
    /// The gender models.
    /// </summary>
    public class GenderModels
    {
        /// <summary>
        /// The is initialized.
        /// </summary>
        public bool IsInitialized;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenderModels"/> class.
        /// </summary>
        public GenderModels()
        {
            this.IsInitialized = true;
        }

        /// <summary>
        /// The get genders.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Gender> GetGenders()
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                // Creating a sqlcommand-object and gives it information about which Stored Procedure to use
                SqlCommand sqlCommand = new SqlCommand("Valg_GetGenders", sqlConnection);

                // Adding parameter to procedure
                sqlCommand.Parameters.Add(
                    new SqlParameter("@GenderId", DBNull.Value));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                List<Gender> genders = new List<Gender>();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            int genderId = 0;
                            if (sqlDataReader["GenderId"] != DBNull.Value)
                            {
                                genderId = Convert.ToInt32(sqlDataReader["GenderId"]);
                            }

                            Gender gender = new Gender
                                                {
                                                    GenderId = genderId,
                                                    GenderName = sqlDataReader["GenderName"].ToString()
                                                };

                            genders.Add(gender);
                        }
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();

                return genders;
            }
        }
    }
}