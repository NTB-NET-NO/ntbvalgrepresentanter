﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ElectionCandidates.Classes;

namespace ElectionCandidates.Models.DataModels
{
    /// <summary>
    ///     This class is used to get statistics data from database
    /// </summary>
    public class CandidateDataModel
    {
        public void TruncateCandidateTempTable()
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("Statistikk_TruncateCandidateStatistics", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();


                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        public void InsertCandidateIdIntoTemptable(string candidateId)
        {
            // We are receiving a string which looks like this
            // 1,2,3,4,5,6
            // So we split it
            string[] candidateIds = candidateId.Split(',');

            // There seems to not be a need for this check
            //if (!candidateId.Contains(",") && (candidateId.Length > 0 || candidateId.Length < 4))
            //{
            //    candidateIds = new[] {candidateId};
            //}

            // If we don't get a string-array, then we return
            if (!candidateId.Any())
            {
                return;
            }

            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                // Looping over the strings
                foreach (string id in candidateIds)
                {
                    var sqlCommand = new SqlCommand("Statistikk_InsertCandidateId", sqlConnection);
                    sqlCommand.Parameters.Add(new SqlParameter("@CandidateId", Convert.ToInt32(id)));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        public List<Statistic> GetGendersFromCollection(string collection, int electionYear)
        {
            var statistics = new List<Statistic>();
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("[Statistikk_GetGendersForCandidates]", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", electionYear));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader dataReader = sqlCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        Statistic stats = new Statistic
                            {
                                Name = dataReader["Gender"].ToString(),
                                Value = Convert.ToInt32(dataReader["Value"])
                            };
                        
                        statistics.Add(stats);
                    }
                }
                return statistics;
            }
        }

        public List<Statistic> GetAgeStairFromCollection(string collection, int electionYear)
        {
            var ages = new List<int>();
            var candidates = new List<Candidate>();

            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("[Statistikk_GetAgesForCandidates]", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", electionYear));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader dataReader = sqlCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        var candidate = new Candidate
                            {
                                Age = Convert.ToInt32(dataReader["AgeYearsIntRound"]),
                                BirthDate = Convert.ToDateTime(dataReader["BirthDate"])
                            };
                        candidates.Add(candidate);
                    }
                }
                
                //List<Statistic> statistics = (from c in candidates
                //                               where c.BirthDate != null
                //                               group c by 10*(c.Age/10)
                //                               into gc
                //                               select new Statistic()
                //                                   {
                //                                       Name = gc.Key.ToString(),
                //                                       Value = gc.Count()
                //                                   }
                //                              ).ToList();

                List<Statistic> statistics = (from c in candidates
                                              where c.BirthDate != null
                                              group c by c.Age
                                                  into gc
                                                  select new Statistic()
                                                  {
                                                      Name = gc.Key.ToString(),
                                                      Value = gc.Count()
                                                  }
                                              ).ToList();
                
                

                return statistics;
            }

            return null;
        }

        public List<Statistic> GetAgeFromCollection(string collection, int electionYear)
        {
            var ages = new List<int>();

            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("[Statistikk_GetAgesForCandidates]", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", electionYear));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader dataReader = sqlCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        int age = Convert.ToInt32(dataReader["AgeYearsIntRound"]);
                        ages.Add(age);
                    }
                }

                // Getting the average age
                int value = Convert.ToInt32(ages.Average());

                Statistic statistic = new Statistic
                    {
                        Name = "Gjennomsnittsalder",
                        Value = value
                    };

                List<Statistic> statistics = new List<Statistic>
                    {
                        statistic
                    };

                return statistics;
            }

            return null;
        }

        public List<Statistic> GetProfessionsFromCollection(string collection, int electionYear)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("[Statistikk_GetProfessionForCandidates]", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", electionYear));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader dataReader = sqlCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    var statistics = new List<Statistic>();
                    while (dataReader.Read())
                    {
                        var stats = new Statistic
                        {
                            Name = dataReader["Profession"].ToString(),
                            Value = Convert.ToInt32(dataReader["Value"])
                        };
                        statistics.Add(stats);
                    }

                    return statistics;
                }

                return null;
            }
        }

        public List<Statistic> GetCountiesFromCollection(string collection, int electionYear)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("[Statistikk_GetCountiesForCandidates]", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", electionYear));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader dataReader = sqlCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    var statistics = new List<Statistic>();
                    while (dataReader.Read())
                    {
                        var stats = new Statistic
                        {
                            Name = dataReader["County"].ToString(),
                            Value = Convert.ToInt32(dataReader["Value"])
                        };
                        statistics.Add(stats);
                    }

                    return statistics;
                }

                return null;
            }
        }

        public List<Statistic> GetSocialMediasFromCollection(string candidates, int electionYear)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("[Statistikk_GetNumberOfSocialMedias]", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", electionYear));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader dataReader = sqlCommand.ExecuteReader();
                if (dataReader.HasRows)
                {
                    var statistics = new List<Statistic>();
                    while (dataReader.Read())
                    {
                        var stats = new Statistic
                            {
                                Name = dataReader["Name"].ToString(),
                                Value = Convert.ToInt32(dataReader["Value"])
                            };
                        statistics.Add(stats);
                    }

                    return statistics;
                }

                return null;
            }
        }
    }
}