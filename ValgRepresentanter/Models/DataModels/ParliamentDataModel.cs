﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ParliamentDataModel.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Models.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    
    using ElectionCandidates.Classes;

    using log4net;

    /// <summary>
    /// The parliament data model.
    /// </summary>
    public class ParliamentDataModel
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(ParliamentDataModel));

        /// <summary>
        /// Initializes a new instance of the <see cref="ParliamentDataModel"/> class.
        /// </summary>
        public ParliamentDataModel()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// The get current parliament period.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ParliamentPeriod> GetCurrentParliamentPeriod()
        {
            try
            {
                using (
                    SqlConnection sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand("[Valg_GetCurrentParliamentPeriod]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@Date", DateTime.Today));

                    sqlConnection.Open();

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    List<ParliamentPeriod> parliamentPeriods = new List<ParliamentPeriod>();

                    while (sqlDataReader.Read())
                    {
                        ParliamentPeriod parliamentPeriod = new ParliamentPeriod();
                        if (sqlDataReader["DateFrom"] != DBNull.Value)
                        {
                            parliamentPeriod.DateFrom = Convert.ToDateTime(sqlDataReader["DateFrom"].ToString());
                        }

                        if (sqlDataReader["DateTo"] != DBNull.Value)
                        {
                            parliamentPeriod.DateTo = Convert.ToDateTime(sqlDataReader["DateTo"].ToString());
                        }

                        if (sqlDataReader["Period"] != DBNull.Value)
                        {
                            parliamentPeriod.ParliamentPeriodId = sqlDataReader["Period"].ToString();
                        }

                        parliamentPeriods.Add(parliamentPeriod);
                    }
                    return parliamentPeriods;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return null;
            }
        }

        /// <summary>
        /// The get parliament periods.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<ParliamentPeriod> GetParliamentPeriods()
        {
            try
            {
                using (
                    SqlConnection sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand("Valg_GetParliamentPeriods", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    sqlConnection.Open();

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    List<ParliamentPeriod> parliamentPeriods = new List<ParliamentPeriod>();

                    while (sqlDataReader.Read())
                    {
                        ParliamentPeriod parliamentPeriod = new ParliamentPeriod();
                        if (sqlDataReader["DateFrom"] != DBNull.Value)
                        {
                            parliamentPeriod.DateFrom = Convert.ToDateTime(sqlDataReader["DateFrom"].ToString());
                        }

                        if (sqlDataReader["DateTo"] != DBNull.Value)
                        {
                            parliamentPeriod.DateTo = Convert.ToDateTime(sqlDataReader["DateTo"].ToString());
                        }

                        if (sqlDataReader["Period"] != DBNull.Value)
                        {
                            parliamentPeriod.ParliamentPeriodId = sqlDataReader["Period"].ToString();
                        }

                        parliamentPeriods.Add(parliamentPeriod);
                    }
                    return parliamentPeriods;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return null;
            }
        }

        /// <summary>
        /// The insert parliament periods.
        /// </summary>
        /// <param name="parliamentPeriods">
        /// The parliament Periods.
        /// </param>
        public void InsertParliamentPeriods(List<ParliamentPeriod> parliamentPeriods)
        {
            try
            {
                using (
                    SqlConnection sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand("Valg_TruncateParliamentPeriods", sqlConnection)
                            {
                                CommandType = CommandType.StoredProcedure
                            };

                    sqlConnection.Open();

                    sqlCommand.ExecuteNonQuery();

                    foreach (ParliamentPeriod parliamentPeriod in parliamentPeriods)
                    {
                        sqlCommand = new SqlCommand("Valg_InsertParliamentPeriods", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        sqlCommand.Parameters.Add(new SqlParameter("@DateFrom", parliamentPeriod.DateFrom));

                        sqlCommand.Parameters.Add(new SqlParameter("@DateTo", parliamentPeriod.DateTo));

                        sqlCommand.Parameters.Add(new SqlParameter("@ParliamentPeriodId", parliamentPeriod.ParliamentPeriodId));

                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        /// The get parliament periods for candidate.
        /// </summary>
        /// <param name="candidateId">
        /// The candidate id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ParliamentPeriod> GetParliamentPeriodsForCandidate(int candidateId)
        {
            Logger.Debug("Candidate id: " + candidateId);

            if (candidateId == 0)
            {
                throw new Exception("Cannot proceed. CandidateId is not set!");
            }
            try
            {
                using (
                    SqlConnection sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand("Valg_GetParliamentPeriodsByCandidateId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@CandidateId", candidateId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new List<ParliamentPeriod>();
                    }

                    List<ParliamentPeriod> parliamentPeriods = new List<ParliamentPeriod>();
                    while (sqlDataReader.Read())
                    {
                        ParliamentPeriod parliamentPeriod = new ParliamentPeriod();
                        
                        if (sqlDataReader["DateFrom"] != DBNull.Value)
                        {
                            parliamentPeriod.DateFrom = Convert.ToDateTime(sqlDataReader["DateFrom"].ToString());
                        }

                        if (sqlDataReader["DateTo"] != DBNull.Value)
                        {
                            parliamentPeriod.DateTo = Convert.ToDateTime(sqlDataReader["DateTo"].ToString());
                        }

                        if (sqlDataReader["Period"] != DBNull.Value)
                        {
                            parliamentPeriod.ParliamentPeriodId = sqlDataReader["Period"].ToString();
                        }

                        parliamentPeriods.Add(parliamentPeriod);
                    }

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }

                    return parliamentPeriods;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                throw new Exception(exception.Message);
                return null;
            }
        }
    }
}