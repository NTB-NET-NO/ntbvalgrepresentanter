﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ToolsModels.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The tools models.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Models
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Security.Policy;
    using System.Web;
    using System.Xml;

    using ElectionCandidates.Classes;
    using ElectionCandidates.Models.DataModels;

    using log4net;

    /// <summary>
    /// The tools models.
    /// </summary>
    public class ToolsModels
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(ToolsModels));

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolsModels"/> class.
        /// </summary>
        public ToolsModels()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// The get current parliament members from parliament.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Candidate> GetCurrentParliamentMembersFromParliament()
        {
            XmlDocument xmlDocument = new XmlDocument();

            string url = ConfigurationManager.AppSettings["ParliamentDataStore"] + "/eksport/dagensrepresentanter";

            // Now loading the XML from the datasource
            xmlDocument.Load(url);

            XmlNamespaceManager namespaceManager = new XmlNamespaceManager(xmlDocument.NameTable);
            namespaceManager.AddNamespace("data", "http://data.stortinget.no");
            namespaceManager.AddNamespace("i", "http://www.w3.org/2001/XMLSchema-instance");

            XmlNodeList nodeList =
                xmlDocument.SelectNodes(
                    "//data:dagensrepresentanter_liste/data:dagensrepresentant[data:fast_vara_for[@i:nil='true']]",
                    namespaceManager);

            if (nodeList == null)
            {
                return null;
            }

            var candidates = AddCandidatesFromXml(nodeList);

            // Query to get those in power
            nodeList =
                xmlDocument.SelectNodes(
                    "//data:dagensrepresentanter_liste/data:dagensrepresentant/data:fast_vara_for[data:etternavn != '']",
                    namespaceManager);

            if (nodeList == null)
            {
                return null;
            }

            candidates.AddRange(AddCandidatesFromXml(nodeList));
            
            return candidates;
        }

        /// <summary>
        /// The get parliament periods.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ParliamentPeriod> GetParliamentPeriods()
        {
            XmlDocument xmlDocument = new XmlDocument();
            string url = ConfigurationManager.AppSettings["ParliamentDataStore"] + "/eksport/stortingsperioder";
            
            // Now loading the XML from the datasource
            xmlDocument.Load(url);
            
            XmlNamespaceManager namespaceManager = new XmlNamespaceManager(xmlDocument.NameTable);
            namespaceManager.AddNamespace("data", "http://data.stortinget.no");
            

            XmlNodeList nodeList = xmlDocument.SelectNodes("//data:stortingsperioder_liste/data:stortingsperiode", namespaceManager);

            if (nodeList == null)
            {
                return null;
            }

            List<ParliamentPeriod> parliamentPeriods = new List<ParliamentPeriod>();
            foreach (XmlNode node in nodeList)
            {
                // node.NamespaceURI = "http://data.stortinget.no";
                DateTime fromDate = DateTime.Today;
                DateTime toDate = DateTime.Today;
                string id = string.Empty;

                if (node["fra"] != null)
                {
                    fromDate = Convert.ToDateTime(node["fra"].InnerText);
                }

                if (node["til"] != null)
                {
                    toDate = Convert.ToDateTime(node["til"].InnerText);
                }

                if (node["id"] != null)
                {
                    id = node["id"].InnerText;
                }

                // now we are to do something about this
                Logger.Debug("fromDate: " + fromDate);
                Logger.Debug("toDate: " + toDate);
                Logger.Debug("id: " + id);

                ParliamentPeriod parliamentPeriod = new ParliamentPeriod
                    {
                        DateFrom = fromDate,
                        DateTo = toDate,
                        ParliamentPeriodId = id
                    };
                parliamentPeriods.Add(parliamentPeriod);
            }

            return parliamentPeriods;
        }

        /// <summary>
        /// The match current parliament against candidate list.
        /// </summary>
        /// <param name="candidates">
        /// The candidates.
        /// </param>
        /// <param name="currentCandidates">
        /// The current candidates.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Candidate> MatchCurrentParliamentAgainstCandidateList(List<Candidate> candidates, List<Candidate> currentCandidates)
        {
            ParliamentDataModel parliamentDataModel = new ParliamentDataModel();
            
            List<ParliamentPeriod> parliamentPeriods = parliamentDataModel.GetCurrentParliamentPeriod();
            Logger.Debug("Parliament Periods: " + parliamentPeriods.Count);

            // Current are those candidates that is in parliament today
            List<Candidate> newCandidates = new List<Candidate>();

            foreach (Candidate candidate in candidates)
            {
                Logger.Debug("Working with " + candidate.FirstName + " " + candidate.LastName);
                foreach (Candidate checkCandidate in currentCandidates)
                {
                    if (candidate.FirstName == checkCandidate.FirstName && candidate.LastName == checkCandidate.LastName)
                    {
                        Logger.Debug("Found " + candidate.FirstName + " " + candidate.LastName + " in lists from Parliament");
                        candidate.ParliamentPeriods = parliamentPeriods;
                        
                        if (checkCandidate.BirthDate != null && (candidate.BirthDate != null && candidate.BirthDate.Value.Date != checkCandidate.BirthDate.Value.Date))
                        {
                            Logger.Debug("We are updating birthdate (" + checkCandidate.BirthDate.Value.Date + " <-> " + candidate.BirthDate.Value.Date + ")");
                            candidate.BirthDate = checkCandidate.BirthDate;
                        }
                    }
                }
                
                Logger.Info("Adding candidate " + candidate.FirstName + " " + candidate.LastName);
                newCandidates.Add(candidate);
            }

            return newCandidates;
        }

        /// <summary>
        /// The update candidates.
        /// </summary>
        /// <param name="candidates">
        /// The candidates.
        /// </param>
        public void UpdateCandidates(List<Candidate> candidates)
        {
            // We start by getting gender for the Person
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                foreach (Candidate candidate in candidates)
                {
                    SqlCommand sqlCommand = new SqlCommand("[Valg_UpdateCheckedElectionCandidate]", sqlConnection)
                                                {
                                                    CommandType = CommandType.StoredProcedure
                                                };
                    sqlCommand.Parameters.Add(new SqlParameter("@CandidateId", candidate.CandidateId));

                    sqlCommand.Parameters.Add(new SqlParameter("@BirthDate", candidate.BirthDate));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    // Deleting existing parliament periods for this candidate
                    sqlCommand = new SqlCommand("[Valg_DeleteParliamentPeriods]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    }; 
                    sqlCommand.Parameters.Add(new SqlParameter("@CandidateId", candidate.CandidateId));

                    sqlCommand.ExecuteNonQuery();


                    // Now we shall do the parliamentperoids
                    sqlCommand = new SqlCommand("[Valg_SetParliamentPeriods]", sqlConnection)
                                     {
                                         CommandType = CommandType.StoredProcedure
                                     };

                    // Checking if the parliament periods is null or not
                    if (candidate.ParliamentPeriods != null)
                    {
                        foreach (ParliamentPeriod parliamentPeriod in candidate.ParliamentPeriods)
                        {
                            sqlCommand.Parameters.Add(
                                new SqlParameter("@ParliamentPeriodId", parliamentPeriod.ParliamentPeriodId));
                            sqlCommand.Parameters.Add(new SqlParameter("@CandidateId", candidate.CandidateId));

                            if (sqlConnection.State == ConnectionState.Closed)
                            {
                                sqlConnection.Open();
                            }

                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }

                }
            }
        }

        /// <summary>
        /// The update candidate pictures.
        /// </summary>
        /// <param name="candidates">
        /// The candidates.
        /// </param>
        public void UpdateCandidatePictures(List<Candidate> candidates)
        {
            // We start by getting gender for the Person
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                foreach (Candidate candidate in candidates)
                {
                    SqlCommand sqlCommand = new SqlCommand("[Valg_UpdateCandidatePicture]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlCommand.Parameters.Add(new SqlParameter("@CandidateId", candidate.CandidateId));

                    sqlCommand.Parameters.Add(new SqlParameter("@Picture", candidate.Picture));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        /// The add candidates from xml.
        /// </summary>
        /// <param name="nodeList">
        /// The node list.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private static List<Candidate> AddCandidatesFromXml(XmlNodeList nodeList)
        {
            List<Candidate> candidates = new List<Candidate>();

            foreach (XmlNode node in nodeList)
            {
                Candidate candidate = new Candidate();

                if (node["etternavn"] != null)
                {
                    candidate.LastName = node["etternavn"].InnerText;
                }

                if (node["fornavn"] != null)
                {
                    candidate.FirstName = node["fornavn"].InnerText;
                }

                if (node["foedselsdato"] != null)
                {
                    candidate.BirthDate = Convert.ToDateTime(node["foedselsdato"].InnerText);
                }

                if (node["parti"] != null)
                {
                    string currentNode = "<foo>" + node["parti"].InnerXml + "</foo>";
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(currentNode);
                    xmlDocument.NameTable.Add("http://data.stortinget.no");
                    
                    XmlNamespaceManager namespaceManager = new XmlNamespaceManager(xmlDocument.NameTable);
                    namespaceManager.AddNamespace("data", "http://data.stortinget.no");
                    XmlNode selectSingleNode = node.SelectSingleNode("data:parti/data:navn", namespaceManager);
                    if (selectSingleNode != null)
                    {
                        string partinavn = selectSingleNode.InnerText;
                    
                        candidate.PoliticalParty = partinavn;
                    }
                }

                ParliamentPeriod parliamentPeriod = new ParliamentPeriod
                                                        {
                                                            DateFrom = DateTime.Parse("2009-10-01"),
                                                            DateTo = DateTime.Parse("2013-09-30"),
                                                            ParliamentPeriodId = "2009-2013"
                                                        };

                List<ParliamentPeriod> parliamentPeriods = new List<ParliamentPeriod> { parliamentPeriod };
                candidate.ParliamentPeriods = parliamentPeriods;

                candidates.Add(candidate);
            }
            return candidates;
        }

        /// <summary>
        /// The check candidates picture.
        /// </summary>
        /// <param name="candidates">
        /// The candidates.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Candidate> CheckCandidatesPicture(List<Candidate> candidates)
        {
            List<Candidate> correctedList = new List<Candidate>();
            foreach (Candidate candidate in candidates)
            {
                // If the candidate name does not contain org, then we need to check if the candidate picture has been uploaded
                if (candidate.Picture == null)
                {
                    continue;
                }

                if (!candidate.Picture.Contains("_org"))
                {
                    string[] fileNameParts = candidate.Picture.Split('_');
                    string Party = fileNameParts[0];
                    string CountyId = fileNameParts[1];
                    string candidateName = fileNameParts[2];

                    string picturePath = @"/Content/uploads/" + Party + @"/" + CountyId + @"/" + candidate.Picture + "_org.jpg";
                    
                    FileInfo fileInfo = new FileInfo(picturePath);

                    if (!fileInfo.Exists)
                    {
                        string pngPicturePath = @"/Content/uploads/" + Party + @"/" + CountyId + @"/"
                                                + candidate.Picture + "_org.jpg";
                        fileInfo = new FileInfo(pngPicturePath);

                        if (!fileInfo.Exists)
                        {
                            candidate.Picture = string.Empty;
                        }
                        else
                        {
                            candidate.Picture = candidate.Picture + @"_org.png";
                        }
                    }
                    else
                    {
                        candidate.Picture = candidate.Picture + @"_org.jpg";
                    }
                }
                correctedList.Add(candidate);
            }
            return correctedList;
        }
    }
}