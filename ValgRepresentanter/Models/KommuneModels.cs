﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KommuneModels.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the KommuneModels type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Models
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using ElectionCandidates.Classes;

    /// <summary>
    /// The kommune models.
    /// </summary>
    public class KommuneModels
    {
        /// <summary>
        /// The get kommuner.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Kommune> GetKommuner()
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetKommuner", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlConnection.Open();

                List<Kommune> kommuner = new List<Kommune>();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            int kretsId = 0;
                            int municipalityId = 0;

                            // Checking that the values from the database isn't null
                            if (sqlDataReader["KretsNummer"] != DBNull.Value)
                            {
                                if (sqlDataReader["KretsNummer"].ToString() != "NULL")
                                {
                                    kretsId = Convert.ToInt32(sqlDataReader["KretsNummer"].ToString());
                                }
                            }

                            // Checking that the values from the database isn't null
                            if (sqlDataReader["KommuneNummer"] != DBNull.Value)
                            {
                                if (sqlDataReader["KommuneNummer"].ToString() != "NULL")
                                {
                                    municipalityId = Convert.ToInt32(sqlDataReader["KommuneNummer"].ToString());
                                }
                            }

                            string municipalityName = sqlDataReader["KommuneNavn"].ToString();

                            Kommune kommune = new Kommune
                                                  {
                                                      KommuneId = municipalityId,
                                                      KretsId = kretsId,
                                                      KommuneNavn = municipalityName
                                                  };
                            
                            kommuner.Add(kommune);
                        }
                    }

                    return kommuner;
                }
            }
        }
    }
}