﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProfessionModels.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the ProfessionModels type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Models
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using ElectionCandidates.Classes;

    /// <summary>
    /// The profession models.
    /// </summary>
    public class ProfessionModels
    {
        /// <summary>
        /// The is initialized.
        /// </summary>
        public bool IsInitialized;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProfessionModels"/> class.
        /// </summary>
        public ProfessionModels()
        {
            this.IsInitialized = true;
        }

        /// <summary>
        /// Gets or sets the profession name.
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// Public method that makes it possible to add professions to the database
        /// </summary>
        public void SetProfessions()
        {
            // We are trimming the string, just to make sure that we are not receiving '    '
            if (ProfessionName.Trim() == string.Empty)
            {
                throw new Exception("Cannot insert new profession. No value!");
            }

            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_SetProfession", sqlConnection);

                sqlCommand.Parameters.Add(
                    new SqlParameter("@Profession", ProfessionName));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();
            }
        }

        /// <summary>
        /// Method that returns all professions that we have in the database.
        /// </summary>
        /// <returns>Returns list of professions</returns>
        public List<Profession> GetProfessions()
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetProfessions", sqlConnection)
                                            {
                                                CommandType = CommandType.StoredProcedure
                                            };

                sqlConnection.Open();

                List<Profession> professions = new List<Profession>();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (!sqlDataReader.HasRows)
                    {
                        throw new Exception("Query returned no rows!");
                    }

                    while (sqlDataReader.Read())
                    {
                        int professionId = 0;

                        string professionName = sqlDataReader["Profession"].ToString();

                        // Checking that the value from the database isn't null
                        if (sqlDataReader["ProfessionId"] != DBNull.Value)
                        {
                            professionId = Convert.ToInt32(sqlDataReader["ProfessionId"]);
                        }

                        // Creating profession object and populates it
                        Profession profession = new Profession
                                                      {
                                                          ProfessionId = professionId,
                                                          ProfessionName = professionName
                                                      };

                        // Adding the profession object to the list
                        professions.Add(profession);
                    }
                }
                
                return professions;
            }
        }
    }
}