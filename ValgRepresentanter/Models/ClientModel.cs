﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClientModel.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the ClientModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Models
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using ElectionCandidates.Classes;

    /// <summary>
    /// The client model.
    /// </summary>
    public class ClientModel 
    {
        /// <summary>
        /// The get client.
        /// </summary>
        /// <param name="clientId">
        /// The client id.
        /// </param>
        /// <returns>
        /// The <see cref="Client"/>.
        /// </returns>
        public Client GetClient(int clientId)
        {
            using (SqlConnection sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                // Creating the sqlCommand object and calling the Stored Procedure
                SqlCommand sqlCommand = new SqlCommand("Valg_GetClientData", sqlConnection);

                // Adding parameters used in the Stored Procedure
                sqlCommand.Parameters.Add(
                        new SqlParameter("@ClientId", clientId));

                // Telling the system that we are using a Stored Procedure CommandType
                sqlCommand.CommandType = CommandType.StoredProcedure;

                // Opening the connection
                sqlConnection.Open();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            Client client = new Client
                                                  {
                                                      ClientId = clientId,
                                                      ClientName = sqlDataReader["sClientName"].ToString(),
                                                      ClientUrl = sqlDataReader["sClientUrl"].ToString(),
                                                      ClientContact = sqlDataReader["sClientContact"].ToString(),
                                                      ClientContactEmail =
                                                          sqlDataReader["sClientContactEmail"].ToString(),
                                                      UserName = sqlDataReader["sClientLoginName"].ToString(),
                                                      PassWord = sqlDataReader["sClientPassword"].ToString(),
                                                      Caption = sqlDataReader["sCaption"].ToString(),
                                                      CSS = sqlDataReader["sCss"].ToString()
                                                  };

                            if (sqlDataReader["iPubliseringsMetode"] != DBNull.Value)
                            {
                                client.PublishMethod = Convert.ToInt32(sqlDataReader["iPubliseringsMetode"]);
                            }

                            if (sqlDataReader["iShowDistrikt"] != DBNull.Value)
                            {
                                client.ShowDistrict = Convert.ToInt32(sqlDataReader["iShowDistrikt"]);
                            }

                            return client;
                        }
                    }
                }

                return null;
            }
        }
    }
}