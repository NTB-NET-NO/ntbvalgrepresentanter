﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CandidateModel.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the CandidateModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Models
{
    using System.Collections.Generic;

    using Classes;

    /// <summary>
    /// The candidate model.
    /// </summary>
    public class CandidateModel
    {
        /// <summary>
        /// Gets or sets the social connections.
        /// </summary>
        public List<SocialConnection> SocialConnections { get; set; }

        /// <summary>
        /// Gets or sets the genders.
        /// </summary>
        public List<Gender> Genders { get; set; }

        /// <summary>
        /// Gets or sets the parties.
        /// </summary>
        public List<Parti> Parties { get; set; }

        /// <summary>
        /// Gets or sets the candidate.
        /// </summary>
        public Candidate Candidate { get; set; }

        /// <summary>
        /// Gets or sets the professions.
        /// </summary>
        public List<Profession> Professions { get; set; }

        /// <summary>
        /// Gets or sets the parliament periods.
        /// </summary>
        public List<ParliamentPeriod> ParliamentPeriods { get; set; }

        /// <summary>
        /// Gets or sets the year of election.
        /// </summary>
        public int Year { get; set; }
    }
}