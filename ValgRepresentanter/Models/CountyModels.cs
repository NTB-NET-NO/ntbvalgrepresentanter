﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CountyModels.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the CountyModels type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Models
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using ElectionCandidates.Classes;

    /// <summary>
    /// The county models.
    /// </summary>
    public class CountyModels
    {
        /// <summary>
        /// The get representant counties.
        /// </summary>
        /// <param name="representantId">
        /// The representant id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<CountyRepresentant> GetRepresentantCounties(int representantId)
        {
            List<CountyRepresentant> countyRepresentantses = new List<CountyRepresentant>();

            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("[Valg_GetFylkeByRepresentantId]", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@RepresentantId", representantId));

                sqlConnection.Open();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            int countyId = 0;
                            int position = 0;
                            int candidateId = 0;
                            
                            if (sqlDataReader["CountyId"] != DBNull.Value)
                            {
                                countyId = Convert.ToInt32(sqlDataReader["CountyId"]);
                            }

                            if (sqlDataReader["CountySeat"] != DBNull.Value)
                            {
                                position = Convert.ToInt32(sqlDataReader["CountySeat"]);
                            }

                            if (sqlDataReader["RepresentantId"] != DBNull.Value)
                            {
                                candidateId = Convert.ToInt32(sqlDataReader["RepresentantId"]);
                            }

                            CountyRepresentant countyRepresentant = new CountyRepresentant
                                {
                                    CountyId = countyId,
                                    CountyName = sqlDataReader["CountyName"].ToString(),
                                    CountyPosition = position,
                                    RepresentantId = candidateId
                                };

                            if (sqlDataReader["ElectionYear"] != DBNull.Value)
                            {
                                countyRepresentant.ElectionYear = Convert.ToInt32(sqlDataReader["ElectionYear"]);
                            }

                            countyRepresentantses.Add(countyRepresentant);
                        }
                    }
                }
            }

            return countyRepresentantses;
        }

        /// <summary>
        /// The get counties.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<County> GetCounties()
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetFylker", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlConnection.Open();

                List<County> counties = new List<County>();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            // Checking that the value from the database isn't null
                            int countyId = 0;
                            if (sqlDataReader["CountyId"] != DBNull.Value)
                            {
                                countyId = Convert.ToInt32(sqlDataReader["CountyId"]);
                            }

                            // Creating a county object and populates it with the information from the database
                            County county = new County
                                {
                                    CountyId = countyId,
                                    CountyName = sqlDataReader["CountyName"].ToString()
                                };

                            // Adding the newly created object to the list of counties
                            counties.Add(county);
                        }
                    }

                    return counties;
                }
            }
        }
    }
}