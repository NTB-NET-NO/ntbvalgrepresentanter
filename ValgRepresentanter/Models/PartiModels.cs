﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PartiModels.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the PartiModels type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Models
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using ElectionCandidates.Classes;

    /// <summary>
    /// The parti models.
    /// </summary>
    public class PartiModels
    {
        /// <summary>
        /// The get parties.
        /// </summary>
        /// <param name="electionYear">
        /// The election Year.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Parti> GetParties(int electionYear)
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetPartier", sqlConnection);
                sqlCommand.Parameters.Add(
                    new SqlParameter("@PartiKode", DBNull.Value));
                sqlCommand.Parameters.Add(
                    new SqlParameter("@RepresentantId", DBNull.Value));

                if (electionYear <= 0)
                {
                    electionYear = DateTime.Today.Year;
                }
                sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", electionYear));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                List<Parti> partier = new List<Parti>();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            string partyCode = string.Empty;
                            string partyName = string.Empty;
                            int partyId = 0;
                            int id = 0;
                            int sortOrder = 0;

                            if (sqlDataReader["PartiKode"] != DBNull.Value)
                            {
                                partyCode = sqlDataReader["PartiKode"].ToString();
                            }

                            if (sqlDataReader["PartiNavn"] != DBNull.Value)
                            {
                                partyName = sqlDataReader["PartiNavn"].ToString();
                            }

                            if (sqlDataReader["PartiId"] != DBNull.Value)
                            {
                                partyId = Convert.ToInt32(sqlDataReader["PartiId"].ToString());
                            }

                            if (sqlDataReader["Id"] != DBNull.Value)
                            {
                                id = Convert.ToInt32(sqlDataReader["Id"].ToString());
                            }

                            if (sqlDataReader["SortSeq"] != DBNull.Value)
                            {
                                sortOrder = Convert.ToInt32(sqlDataReader["SortSeq"].ToString());
                            }

                            Parti parti = new Parti
                                              {
                                                  Code = partyCode,
                                                  Name = partyName,
                                                  Id = id,
                                                  PartiId = partyId,
                                                  SortOrder = sortOrder
                                              };
                            
                            partier.Add(parti);
                        }
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();

                return partier;
            }
        }
    }
}