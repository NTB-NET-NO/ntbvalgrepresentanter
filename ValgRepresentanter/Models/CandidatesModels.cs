﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CandidatesModels.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the CandidatesModels type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Models
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    
    using Classes;

    // Adding support for log4net
    using DataModels;

    using log4net;

    // Adding support for JSON
    using Newtonsoft.Json;

    using File = System.IO.File;

    /// <summary>
    /// The candidates models.
    /// </summary>
    public class CandidatesModels
    {
        /// <summary>
        /// The candidate.
        /// </summary>
        public Candidate Candidate = new Candidate();

        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(CandidatesModels));

        /// <summary>
        /// Initializes a new instance of the <see cref="CandidatesModels"/> class.
        /// </summary>
        public CandidatesModels()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Gets or sets the picture.
        /// </summary>
        public string Picture { get; set; }

        /// <summary>
        /// Gets or sets the form collection.
        /// </summary>
        public FormCollection FormCollection { get; set; }

        /// <summary>
        /// Gets or sets the candidate id.
        /// </summary>
        public int CandidateId { get; set; }

        /// <summary>
        /// The edit candidate.
        /// </summary>
        public void EditCandidate() 
        {
            try
            {
                List<SocialConnection> socialConnections = null;
                
                string[] socialMediaUserName = null;

                string[] socialMedia = new string[] { };
                if (FormCollection["sosialemedier[]"] != null)
                {
                    socialMedia = FormCollection["sosialemedier[]"].Split(',');
                }

                if (FormCollection["sm_username[]"] != null)
                {
                    string usernames = FormCollection["sm_username[]"];
                    usernames = usernames.Replace(",,", ",");
                    socialMediaUserName = usernames.Split(',');
                }

                if (socialMedia.Any())
                {
                    socialConnections = new List<SocialConnection>();
                    int counter = 0;
                    foreach (var socialId in socialMedia)
                    {
                        if (socialMediaUserName != null)
                        {
                            SocialConnection socialConnection = new SocialConnection
                                {
                                    Name = socialMedia[counter],
                                    Id = Convert.ToInt32(socialId)
                                };

                            // If counter is empty, try one up.. 
                            if (socialMediaUserName[counter] == string.Empty)
                            {
                                socialConnection.UserName = socialMediaUserName[counter + 1];
                            }

                            socialConnections.Add(socialConnection);
                        }

                        counter++;
                    }
                }

                Candidate.SocialConnections = socialConnections;
                
                // Debugging information - checking collection values
                foreach (string key in FormCollection.Keys)
                {
                    Logger.Debug("Key: " + key + " - " + FormCollection[key]);
                }
                
                Candidate.CandidateId = CandidateId;
                Candidate.FirstName = FormCollection["Firstname"];
                Candidate.LastName = FormCollection["Lastname"];

                Candidate.ProfessionId = Convert.ToInt32(FormCollection["Professions"]);
                if (FormCollection["Professions"] == string.Empty)
                {
                    Candidate.Profession = FormCollection["Occupation"];
                }
                Candidate.HomeCity = FormCollection["HomeCity"];

                Candidate.MunicipalityId = Convert.ToInt32(FormCollection["Kommune"]);

                Candidate.Age = Convert.ToInt32(FormCollection["Age"]);
                if (FormCollection["BirthDate"] != string.Empty)
                {
                    Candidate.BirthDate = Convert.ToDateTime(FormCollection["BirthDate"]);
                }

                Candidate.Description = FormCollection["Description"];
                Candidate.Education = FormCollection["Education"];
                Candidate.ElectionYear = Convert.ToInt32(FormCollection["ElectionYear"]);
                Candidate.Email = FormCollection["email"];
                Candidate.PoliticalPartyCode = FormCollection["PoliticalParty"];
                Candidate.GenderId = Convert.ToInt32(FormCollection["gender"]);
                Candidate.SocialConnections = socialConnections;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        /// The update representant.
        /// </summary>
        public void UpdateCandidate()
        {
            // We start by getting gender for the Person
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_UpdateElectionCandidate", sqlConnection);
                sqlCommand.Parameters.Add(new SqlParameter("@CandidateId", Candidate.CandidateId));
                sqlCommand.Parameters.Add(new SqlParameter("@FirstName", Candidate.FirstName));

                sqlCommand.Parameters.Add(new SqlParameter("@LastName", Candidate.LastName));

                sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", Candidate.MunicipalityId));

                if (Candidate.BirthDate > DateTime.MinValue)
                {
                    var birthDate = Candidate.BirthDate;
                    if (birthDate != null)
                    {
                        sqlCommand.Parameters.Add(new SqlParameter("@BirthDate", birthDate.Value.Date));
                    }
                }

                sqlCommand.Parameters.Add(new SqlParameter("@Gender", Candidate.GenderId));

                sqlCommand.Parameters.Add(new SqlParameter("@Occupation", Candidate.ProfessionId));

                sqlCommand.Parameters.Add(new SqlParameter("@Education", Candidate.Education));

                sqlCommand.Parameters.Add(new SqlParameter("@Description", Candidate.Description));

                Logger.Debug("We are updating picture: '" + Candidate.Picture + "'");
                if (Candidate.Picture != null)
                {
                    string candidatePicture = Candidate.Picture;
                    // Adding this test just in case
                    if (candidatePicture.Trim() != string.Empty)
                    {
                        Logger.Debug("We are REALLY updating picture: '" + candidatePicture + "'");
                        sqlCommand.Parameters.Add(new SqlParameter("@Picture", candidatePicture));
                    }
                }

                sqlCommand.Parameters.Add(new SqlParameter("@Age", Candidate.Age));

                sqlCommand.Parameters.Add(new SqlParameter("@HomeCity", Candidate.HomeCity));

                sqlCommand.Parameters.Add(new SqlParameter("@Email", Candidate.Email));

                sqlCommand.Parameters.Add(new SqlParameter("@PoliticalParty", Candidate.PoliticalPartyCode));
                
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();
                try
                {
                    sqlCommand.ExecuteNonQuery();

                    sqlConnection.Close();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
            
                // now we shall update (or add) the social connections for this candidate
                if (Candidate.SocialConnections != null)
                {
                    sqlCommand = new SqlCommand("DeleteSocialConnectionsById", sqlConnection)
                                     {
                                         CommandType = CommandType.StoredProcedure
                                     };
                    sqlCommand.Parameters.Add(new SqlParameter("@CandidateId", Candidate.CandidateId));
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);
                    }

                    foreach (SocialConnection socialConnection in Candidate.SocialConnections)
                    {
                        sqlCommand = new SqlCommand("Valg_SetSocialConnection", sqlConnection);
                        sqlCommand.Parameters.Add(new SqlParameter("@RepresentantId", Candidate.CandidateId));
                        sqlCommand.Parameters.Add(new SqlParameter("@SocialConnectionId", socialConnection.Id));
                        sqlCommand.Parameters.Add(new SqlParameter("@Username", socialConnection.UserName));

                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        if (sqlConnection.State == ConnectionState.Closed)
                        {
                            sqlConnection.Open();
                        }
                        try
                        {
                            sqlCommand.ExecuteNonQuery();
                        }
                        catch (Exception exception)
                        {
                            Logger.Error(exception.Message);
                            Logger.Error(exception.StackTrace);
                        }
                    }
                }

                if (Candidate.ParliamentPeriods != null)
                {
                    // Now we shall first delete the parliament periods for this candidate, then we add the "new ones"
                    sqlCommand = new SqlCommand("Valg_DeleteParliamentPeriods", sqlConnection)
                                     {
                                         CommandType =
                                             CommandType
                                             .StoredProcedure
                                     };
                    sqlCommand.Parameters.Add(new SqlParameter("@CandidateId", Candidate.CandidateId));

                    // Checking if sql connection state is closed or not
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    /*
                     * We won't close the connection now
                     */

                    foreach (ParliamentPeriod parliamentPeriod in Candidate.ParliamentPeriods)
                    {
                        sqlCommand = new SqlCommand("Valg_SetParliamentPeriods", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        sqlCommand.Parameters.Add(new SqlParameter("@CandidateId", Candidate.CandidateId));
                        sqlCommand.Parameters.Add(
                            new SqlParameter("@ParliamentPeriodId", parliamentPeriod.ParliamentPeriodId));

                        sqlCommand.ExecuteNonQuery();
                    }
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        /// Method that creates a representantmodel and returns it
        /// </summary>
        /// <param name="candidateId">
        /// The id for the candidate
        /// </param>
        /// <param name="electionYear">
        /// The election Year.
        /// </param>
        /// <returns>
        /// CandidateModel is returned
        /// </returns>
        public CandidateModel GetRepresentantModel(int candidateId, int electionYear)
        {
            // todo: Change this method so that it returns a Candidate object rather than a CandidateModel
            GenderModels genderModels = new GenderModels();
            SocialConnectionModel socialConnectionModel = new SocialConnectionModel();
            ProfessionModels professionModels = new ProfessionModels();
            PartiModels partiModels = new PartiModels();
            ParliamentDataModel parliamentDataModel = new ParliamentDataModel();

            Candidate candidate = GetRepresentant(candidateId);
            List<Gender> genders = new List<Gender>(genderModels.GetGenders());
            List<SocialConnection> socialConnections = new List<SocialConnection>(socialConnectionModel.GetSocialConnections());

            // Getting the list of professions from the database
            List<Profession> professions = new List<Profession>(professionModels.GetProfessions());

            List<Parti> parties = new List<Parti>(partiModels.GetParties(electionYear));
            List<ParliamentPeriod> parliamentPeriods = new List<ParliamentPeriod>(parliamentDataModel.GetParliamentPeriodsForCandidate(candidateId));

            CandidateModel candidateModel = new CandidateModel
                {
                    SocialConnections = socialConnections,
                    Parties = parties,
                    Genders = genders,
                    Candidate = candidate,
                    Professions = professions,
                    ParliamentPeriods = parliamentPeriods
                };
            return candidateModel;
        }

        /// <summary>
        /// The get all political parties.
        /// </summary>
        /// <param name="electionYear">
        /// The election Year.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>Dictionary</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Parti> GetAllPoliticalParties(int electionYear)
        {
            try
            {
                using (
                    SqlConnection sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand("[Valg_GetPartier]", sqlConnection);

                    // We are sending in null values to the Stored Procedure, this so that we get all parties
                    sqlCommand.Parameters.Add(new SqlParameter("@PartiKode", DBNull.Value));
                    sqlCommand.Parameters.Add(new SqlParameter("@RepresentantId", DBNull.Value));
                    sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", DBNull.Value));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlConnection.Open();
                    List<Parti> politicalParties = new List<Parti>();
                    using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                    {
                        if (sqlDataReader.HasRows)
                        {
                            while (sqlDataReader.Read())
                            {
                                Parti parti = new Parti
                                    {
                                        Code = sqlDataReader["PartiKode"].ToString(),
                                        Name = sqlDataReader["PartiNavn"].ToString()
                                    };
                                politicalParties.Add(parti);
                            }
                        }
                    }
                    return politicalParties;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Method that returns one Political Party Member information
        /// </summary>
        /// <param name="candidateId">Integer to tell which member to get from the database</param>
        /// <returns>A Candidate object</returns>
        public Candidate GetRepresentant(int candidateId)
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetValgRepresentanter", sqlConnection);
                sqlCommand.Parameters.Add(new SqlParameter("@RepresentantID", candidateId));
                
                if ((int)HttpContext.Current.Session["Year"] > 0)
                {
                    int electionYear = (int)HttpContext.Current.Session["Year"];
                    sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", electionYear));
                }
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                Candidate candidate = new Candidate();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            candidate.CandidateId = Convert.ToInt32(sqlDataReader["RepresentantId"]);
                            candidate.FirstName = sqlDataReader["FirstName"].ToString();
                            candidate.LastName = sqlDataReader["LastName"].ToString();
                            if (sqlDataReader["MunicipalityId"] != DBNull.Value)
                            {
                                candidate.MunicipalityId = Convert.ToInt32(sqlDataReader["MunicipalityId"]);
                            }

                            if (sqlDataReader["BirthDate"] != DBNull.Value)
                            {
                                candidate.BirthDate = Convert.ToDateTime(sqlDataReader["BirthDate"]);
                            }

                            if (sqlDataReader["Gender"] != DBNull.Value)
                            {
                                candidate.GenderId = Convert.ToInt32(sqlDataReader["Gender"]);
                            }

                            if (sqlDataReader["GenderName"] != DBNull.Value)
                            {
                                candidate.GenderName = sqlDataReader["GenderName"].ToString();
                            }

                            if (sqlDataReader["PartiKode"] != DBNull.Value)
                            {
                                candidate.PoliticalPartyCode = sqlDataReader["PartiKode"].ToString();
                            }

                            if (sqlDataReader["PartiNavn"] != DBNull.Value)
                            {
                                candidate.PoliticalParty = sqlDataReader["PartiNavn"].ToString();
                            }

                            candidate.HomeCity = sqlDataReader["HomeCity"].ToString();

                            if (sqlDataReader["Occupation"] != DBNull.Value)
                            {
                                candidate.ProfessionId = Convert.ToInt32(sqlDataReader["Occupation"].ToString());
                            }
                            
                            if (sqlDataReader["Profession"] != DBNull.Value)
                            {
                                candidate.Profession = sqlDataReader["Profession"].ToString();
                            }

                            if (sqlDataReader["Education"] != DBNull.Value)
                            {
                                candidate.Education = sqlDataReader["Education"].ToString();
                            }

                            if (sqlDataReader["Description"] != DBNull.Value)
                            {
                                candidate.Description = sqlDataReader["Description"].ToString();
                            }

                            if (sqlDataReader["Picture"] != DBNull.Value)
                            {
                                candidate.Picture = sqlDataReader["Picture"].ToString();
                            }

                            if (sqlDataReader["Email"] != DBNull.Value)
                            {
                                candidate.Email = sqlDataReader["Email"].ToString();
                            }
                            
                            if (sqlDataReader["CountyId"] != DBNull.Value)
                            {
                                candidate.County = sqlDataReader["CountyId"].ToString();
                            }
                            
                            if (sqlDataReader["Age"] != DBNull.Value)
                            {
                                candidate.Age = Convert.ToInt32(sqlDataReader["Age"].ToString());
                            }
                            
                            if (sqlDataReader["HomeCity"] != DBNull.Value)
                            {
                                candidate.HomeCity = sqlDataReader["HomeCity"].ToString();
                            }

                            if (sqlDataReader["Kommune"] != DBNull.Value)
                            {
                                candidate.Municipality = sqlDataReader["Kommune"].ToString();
                            }

                            List<CountyRepresentant> counties = new List<CountyRepresentant>();

                            // Now we shall get the counties
                            SqlCommand sqlCommandFylke = new SqlCommand("Valg_GetFylke", sqlConnection);

                            // Adding parameters to SQL-Command
                            sqlCommandFylke.Parameters.Add(
                                new SqlParameter("@RepresentantId", candidateId));
                            sqlCommandFylke.CommandType = CommandType.StoredProcedure;

                            using (SqlDataReader fylkeReader = sqlCommandFylke.ExecuteReader())
                            {
                                if (fylkeReader.HasRows)
                                {
                                    while (fylkeReader.Read())
                                    {
                                        CountyRepresentant county = new CountyRepresentant
                                        {
                                            CountyId = Convert.ToInt32(fylkeReader["FylkeId"].ToString()),
                                            CountyPosition = Convert.ToInt32(fylkeReader["FylkePlass"].ToString())
                                        };
                                        counties.Add(county);
                                    }
                                }
                            }

                            // We need to create code to get the social connections - as they might be more than one
                            List<SocialConnection> socialConnections = new List<SocialConnection>();

                            // Creating the Query-part
                            SqlCommand sqlCommandSocialConnections = new SqlCommand("Valg_GetSocialConnections", sqlConnection);

                            sqlCommandSocialConnections.Parameters.Add(
                                new SqlParameter("@SocialConnectionId", DBNull.Value));
                            sqlCommandSocialConnections.Parameters.Add(
                                new SqlParameter("@RepresentantId", candidateId));

                            sqlCommandSocialConnections.CommandType = CommandType.StoredProcedure;

                            using (SqlDataReader dataReader = sqlCommandSocialConnections.ExecuteReader())
                            {
                                if (!dataReader.HasRows)
                                {
                                    continue;
                                }
                                while (dataReader.Read())
                                {
                                    SocialConnection socialConnection = new SocialConnection
                                                                            {
                                                                                Id = Convert.ToInt32(dataReader["SocialConnectionId"].ToString()),
                                                                                UserName = dataReader["UserName"].ToString(),
                                                                                Name = dataReader["Name"].ToString()
                                                                            };

                                    socialConnections.Add(socialConnection);
                                }
                            }
                            candidate.Counties = counties;
                            candidate.SocialConnections = socialConnections;
                        }
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();

                return candidate;
            }
        }

        /// <summary>
        /// The get social connections by candidate id.
        /// </summary>
        /// <param name="candidateId">
        /// The candidate id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<SocialConnection> GetSocialConnectionsByCandidateId(int candidateId)
        {
            // We need to create code to get the social connections - as they might be more than one
            List<SocialConnection> socialConnections = new List<SocialConnection>();

            // Creating the Query-part
            using (
                SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetSocialConnections", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@SocialConnectionId", DBNull.Value));
                sqlCommand.Parameters.Add(new SqlParameter("@RepresentantId", candidateId));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                using (SqlDataReader dataReader = sqlCommand.ExecuteReader())
                {
                    if (!dataReader.HasRows)
                    {
                        return null;
                    }

                    while (dataReader.Read())
                    {
                        SocialConnection socialConnection = new SocialConnection();

                        if (dataReader["SocialConnectionId"] != DBNull.Value)
                        {
                            socialConnection.Id = Convert.ToInt32(dataReader["SocialConnectionId"].ToString());
                        }

                        if (dataReader["UserName"] != DBNull.Value)
                        {
                            socialConnection.UserName = dataReader["UserName"].ToString();
                        }

                        if (dataReader["Name"] != DBNull.Value)
                        {
                            socialConnection.Name = dataReader["Name"].ToString();
                        }
                            
                        socialConnections.Add(socialConnection);
                    }
                }
                return socialConnections;
            }
        }

        /// <summary>
        /// Method to return a list of political party members from the database
        /// </summary>
        /// <param name="electionYear">
        /// The election Year.
        /// </param>
        /// <returns>
        /// A list of Candidate objects
        /// </returns>
        public List<Candidate> GetRepresentanter(int electionYear)
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetValgRepresentanter", sqlConnection);
                sqlCommand.Parameters.Add(
                    new SqlParameter("@RepresentantID", DBNull.Value));

                sqlCommand.Parameters.Add(
                    new SqlParameter("@ElectionYear", electionYear));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                List<Candidate> valgRepresentanter = new List<Candidate>();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        Candidate candidate = new Candidate();

                        if (sqlDataReader["RepresentantId"] != DBNull.Value)
                        {
                            candidate.CandidateId = (int)sqlDataReader["RepresentantId"];
                        }

                        if (sqlDataReader["FirstName"] != DBNull.Value)
                        {
                            candidate.FirstName = (string)sqlDataReader["FirstName"];
                        }

                        if (sqlDataReader["LastName"] != DBNull.Value)
                        {
                            candidate.LastName = (string)sqlDataReader["LastName"];
                        }

                        if (sqlDataReader["GenderId"] != DBNull.Value)
                        {
                            candidate.GenderId = (int)sqlDataReader["GenderId"];
                        }

                        if (sqlDataReader["GenderName"] != DBNull.Value)
                        {
                            candidate.GenderName = (string)sqlDataReader["GenderName"];
                        }

                        if (sqlDataReader["PartiNavn"] != DBNull.Value)
                        {
                            candidate.PoliticalParty = (string)sqlDataReader["PartiNavn"];
                        }

                        if (sqlDataReader["PartiKode"] != DBNull.Value)
                        {
                            candidate.PoliticalPartyCode = (string)sqlDataReader["PartiKode"];
                        }

                        if (sqlDataReader["Picture"] != DBNull.Value)
                        {
                            candidate.Picture = (string)sqlDataReader["Picture"];
                        }

                        // CountyPosition
                        if (sqlDataReader["CountyPosition"] != DBNull.Value)
                        {
                            candidate.CountyPosition = (int)sqlDataReader["CountyPosition"];
                        }

                        if (sqlDataReader["Email"] != DBNull.Value)
                        {
                            candidate.Email = sqlDataReader["Email"].ToString();
                        }

                        if (sqlDataReader["Description"] != DBNull.Value)
                        {
                            candidate.Description = sqlDataReader["Description"].ToString();
                        }

                        if (sqlDataReader["Education"] != DBNull.Value)
                        {
                            candidate.Education = sqlDataReader["Education"].ToString();
                        }

                        if (sqlDataReader["Age"] != DBNull.Value)
                        {
                            candidate.Age = Convert.ToInt32(sqlDataReader["Age"].ToString());
                        }

                        if (sqlDataReader["Occupation"] != DBNull.Value)
                        {
                            candidate.Profession = sqlDataReader["Occupation"].ToString();
                        }

                        if (sqlDataReader["HomeCity"] != DBNull.Value)
                        {
                            candidate.HomeCity = sqlDataReader["HomeCity"].ToString();
                        }

                        if (sqlDataReader["BirthDate"] != DBNull.Value)
                        {
                            candidate.BirthDate = Convert.ToDateTime(sqlDataReader["BirthDate"].ToString());
                        }

                        if (sqlDataReader["CountyName"] != DBNull.Value)
                        {
                            candidate.County = sqlDataReader["CountyName"].ToString();
                        }

                        if (sqlDataReader["Kommune"] != DBNull.Value)
                        {
                            candidate.Municipality = sqlDataReader["Kommune"].ToString();
                        }

                        if (sqlDataReader["Picture"] != DBNull.Value)
                        {
                            candidate.Picture = sqlDataReader["Picture"].ToString();
                        }

                        candidate.ElectionYear = electionYear;

                        valgRepresentanter.Add(candidate);
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();

                return valgRepresentanter;
            }
        }

        /// <summary>
        /// The get representanter by parti code.
        /// </summary>
        /// <param name="partiCode">
        /// The parti code.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Candidate> GetRepresentanterByPartiCode(string partiCode)
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetValgRepresentanterByPartiCode", sqlConnection);
                sqlCommand.Parameters.Add(
                    new SqlParameter("@PartiCode", partiCode));
                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                List<Candidate> electionCandidate = new List<Candidate>();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        Candidate candidate = new Candidate();

                        if (sqlDataReader["FirstName"] == DBNull.Value)
                        {
                            continue;
                        }

                        if (sqlDataReader["RepresentantId"] != DBNull.Value)
                        {
                            candidate.CandidateId = (int)sqlDataReader["RepresentantId"];
                        }

                        if (sqlDataReader["FirstName"] != DBNull.Value)
                        {
                            candidate.FirstName = (string)sqlDataReader["FirstName"];
                        }

                        if (sqlDataReader["LastName"] != DBNull.Value)
                        {
                            candidate.LastName = (string)sqlDataReader["LastName"];
                        }

                        if (sqlDataReader["GenderName"] != DBNull.Value)
                        {
                            candidate.GenderName = (string)sqlDataReader["GenderName"];
                        }

                        if (sqlDataReader["GenderId"] != DBNull.Value)
                        {
                            candidate.GenderId = (int)sqlDataReader["GenderId"];
                        }

                        if (sqlDataReader["PartiNavn"] != DBNull.Value)
                        {
                            candidate.PoliticalParty = (string)sqlDataReader["PartiNavn"];
                        }

                        if (sqlDataReader["PartiKode"] != DBNull.Value)
                        {
                            candidate.PoliticalPartyCode = (string)sqlDataReader["PartiKode"];
                        }

                        // Adding Election Candidat to ElectionCandidateList
                        electionCandidate.Add(candidate);
                    }

                    sqlDataReader.Close();
                }

                sqlConnection.Close();

                return electionCandidate;
            }
        }

        /// <summary>
        /// The delete representant.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        public void DeleteRepresentant(int id)
        {
            // Valg_DeleteElectionCandidate
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_DeleteElectionCandidate", sqlConnection);
                sqlCommand.Parameters.Add(
                    new SqlParameter("@CandidateId", id));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                sqlCommand.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// The update election data.
        /// </summary>
        /// <param name="representantId">
        /// The representant id.
        /// </param>
        /// <param name="county">
        /// The county.
        /// </param>
        /// <param name="position">
        /// The position.
        /// </param>
        /// <param name="electionYear">
        /// The election year.
        /// </param>
        /// <param name="oldYear">
        /// The old year.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<CandidateData> UpdateElectionData(int representantId, int county, int position, int electionYear, int oldYear)
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_UpdateElectionYearData", sqlConnection);
                sqlCommand.Parameters.Add(
                    new SqlParameter("@RepresentantId", representantId));

                sqlCommand.Parameters.Add(
                    new SqlParameter("@County", county));

                sqlCommand.Parameters.Add(
                    new SqlParameter("@Position", position));

                sqlCommand.Parameters.Add(
                    new SqlParameter("@Year", electionYear));

                sqlCommand.Parameters.Add(
                    new SqlParameter("@OldYear", oldYear));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                List<CandidateData> candidateDatas = new List<CandidateData>();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        int candidateId = 0;
                        int countyId = 0;
                        int countySeat = 0;
                        int thisElectionYear = 0;
                        string countyName = string.Empty;

                        if (sqlDataReader["CandidateId"] != DBNull.Value)
                        {
                            candidateId = Convert.ToInt32(sqlDataReader["CandidateId"]);
                        }

                        if (sqlDataReader["CountyId"] != DBNull.Value)
                        {
                            countyId = Convert.ToInt32(sqlDataReader["CountyId"]);
                        }

                        if (sqlDataReader["CountyName"] != DBNull.Value)
                        {
                            countyName = sqlDataReader["CountyName"].ToString();
                        }

                        if (sqlDataReader["CountySeat"] != DBNull.Value)
                        {
                            countySeat = Convert.ToInt32(sqlDataReader["CountySeat"]);
                        }

                        if (sqlDataReader["ElectionYear"] != DBNull.Value)
                        {
                            thisElectionYear = Convert.ToInt32(sqlDataReader["ElectionYear"]);
                        }

                        CandidateData candidateData = new CandidateData
                            {
                                CandidateId = candidateId,
                                County = countyId.ToString(),
                                CountyName = countyName,
                                Position = countySeat,
                                ElectionYear = thisElectionYear
                            };

                        candidateDatas.Add(candidateData);
                    }
                }

                return candidateDatas;
            }
        }

        /// <summary>
        /// The get election data.
        /// </summary>
        /// <param name="candidateId">
        /// The candidate id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<CandidateData> GetElectionData(int candidateId)
        {
            List<CandidateData> candidateDatas = new List<CandidateData>();

            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetElectionYearData", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@RepresentantId", candidateId));
                DateTime dateTime = new DateTime();
                sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", Convert.ToInt32(dateTime.Year)));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        int countyId = 0;
                        int countySeat = 0;
                        int thisElectionYear = 0;
                        string countyName = string.Empty;

                        if (sqlDataReader["Candidate"] != DBNull.Value)
                        {
                            candidateId = Convert.ToInt32(sqlDataReader["CandidateId"]);
                        }

                        if (sqlDataReader["CountyId"] != DBNull.Value)
                        {
                            countyId = Convert.ToInt32(sqlDataReader["CountyId"]);
                        }

                        if (sqlDataReader["CountyName"] != DBNull.Value)
                        {
                            countyName = sqlDataReader["CountyName"].ToString();
                        }

                        if (sqlDataReader["CountySeat"] != DBNull.Value)
                        {
                            countySeat = Convert.ToInt32(sqlDataReader["CountySeat"]);
                        }

                        if (sqlDataReader["ElectionYear"] != DBNull.Value)
                        {
                            thisElectionYear = Convert.ToInt32(sqlDataReader["ElectionYear"]);
                        }

                        CandidateData candidateData = new CandidateData
                            {
                                CandidateId = candidateId,
                                County = countyId.ToString(),
                                CountyName = countyName,
                                Position = countySeat,
                                ElectionYear = thisElectionYear
                            };

                        candidateDatas.Add(candidateData);
                    }
                }
            }

            return candidateDatas;
        }

        /// <summary>
        /// The generate data file.
        /// </summary>
        /// <param name="electionYear">
        /// The election Year.
        /// </param>
        public void GenerateDataFile(int electionYear)
        {
            // Start by getting All candidates
            List<Candidate> candidates = GetRepresentanter(electionYear);
            List<CandidateShort> candidateShortList = new List<CandidateShort>();

            ParliamentDataModel parliamentDataModel = new ParliamentDataModel();

            // Write out the string
            string path = ConfigurationManager.AppSettings["OutputFolder"];

            if (path == string.Empty)
            {
                return;
            }

            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }

            // Getting parties
            PartiModels partiModels = new PartiModels();
            List<Parti> parties = partiModels.GetParties(DateTime.Today.Year);
            
            FileInfo fi = new FileInfo(path + @"\" + "parties.json");

            if (fi.Exists)
            {
                fi.Delete();
            }

            using (FileStream fs = File.Open(path + @"\" + "parties.json", FileMode.CreateNew))
            using (StreamWriter sw = new StreamWriter(fs))
            using (JsonWriter jw = new JsonTextWriter(sw))
            {
                jw.Formatting = Formatting.Indented;

                JsonSerializer serializer = new JsonSerializer();

                serializer.Serialize(jw, parties);
            }
            foreach (Candidate candidate in candidates)
            {
                // Getting County
                CountyModels countyModels = new CountyModels();
                List<CountyRepresentant> countyRepresentants =
                    countyModels.GetRepresentantCounties(candidate.CandidateId);

                CountyRepresentant filteredCounty = (from foo in countyRepresentants where foo.ElectionYear == DateTime.Today.Year select foo).Single();

                List<SocialConnection> socialConnections = GetSocialConnectionsByCandidateId(candidate.CandidateId);
                if (socialConnections != null)
                {
                    candidate.SocialConnections = socialConnections;
                }

                List<ParliamentPeriod> parliamentPeriods =
                    parliamentDataModel.GetParliamentPeriodsForCandidate(candidate.CandidateId);

                Parti parti = (from p in parties where p.Code == candidate.PoliticalPartyCode select p).Single();
                
                int countyId = filteredCounty.CountyId;
                string countyName = filteredCounty.CountyName;

                DateTime? birthDate = null;
                if (candidate.BirthDate != null)
                {
                    birthDate = candidate.BirthDate.Value.Date;
                }

                if (candidate.PoliticalPartyCode == "A")
                {
                    candidate.PoliticalPartyCode = "Ap";
                }

                if (candidate.Picture != null)
                {
                    if (candidate.Picture.ToLower().Contains("_org.jpg"))
                    {
                        candidate.Picture = candidate.Picture.Replace("_org.jpg", string.Empty);
                        candidate.Picture = candidate.Picture.Replace("_org.JPG", string.Empty);
                    }
                    else if (candidate.Picture.ToLower().Contains("_org.jpeg"))
                    {
                        candidate.Picture = candidate.Picture.Replace("_org.jpeg", string.Empty);
                        candidate.Picture = candidate.Picture.Replace("_org.JPEG", string.Empty);
                    }
                    else if (candidate.Picture.ToLower().Contains("_org.png"))
                    {
                        candidate.Picture = candidate.Picture.Replace("_org.png", string.Empty);
                        candidate.Picture = candidate.Picture.Replace("_org.PNG", string.Empty);
                    }
                }
                else
                {
                    candidate.Picture = string.Empty;
                }

                CandidateLong jsonCandidate = new CandidateLong
                    {
                        CandidateId = candidate.CandidateId,
                        FirstName = candidate.FirstName,
                        LastName = candidate.LastName,
                        DateOfBirth = birthDate,
                        Description = candidate.Description,
                        PoliticalPartyCode = candidate.PoliticalPartyCode,
                        PartiId = parti.PartiId,
                        PartiOrganizationId = parti.Id,
                        PartiSortOrder = parti.SortOrder,
                        Picture = candidate.Picture,
                        ParliamentPeriods = parliamentPeriods,
                        ElectionYear = candidate.ElectionYear,
                        Education = candidate.Education,
                        HomeTown = candidate.HomeCity,
                        Occupation = candidate.Profession,
                        GenderId = candidate.GenderId,
                        GenderName = candidate.GenderName,
                        Age = candidate.Age,
                        Email = candidate.Email,
                        SocialConnections = candidate.SocialConnections,
                        CandidateText = string.Empty,
                        CountyPosition = candidate.CountyPosition,
                        CountyId = filteredCounty.CountyId,
                        CountyName = filteredCounty.CountyName,
                        Municipality = candidate.Municipality
                    };

                CandidateShort jsonCandidateShort = new CandidateShort
                    {
                        CandidateId = candidate.CandidateId,
                        FirstName = candidate.FirstName,
                        LastName = candidate.LastName,
                        Picture = candidate.Picture,
                        PoliticalPartyCode = candidate.PoliticalPartyCode,
                        PartiId = parti.PartiId,
                        PartiOrganizationId = parti.Id,
                        PartiSortOrder = parti.SortOrder,
                        CountyId = countyId,
                        CountyName = countyName,
                        ElectionYear = candidate.ElectionYear,
                        CountyPosition = candidate.CountyPosition,
                        ParliamentPeriods = parliamentPeriods,
                        HomeCity = candidate.HomeCity,
                        Municipality = candidate.Municipality
                    };

                // Add candidateShort object to CandidateShorts-list
                candidateShortList.Add(jsonCandidateShort);

                fi = new FileInfo(path + @"\" + "candidate_" + candidate.CandidateId + "_metainfo.json");

                if (fi.Exists)
                {
                    fi.Delete();
                }

                using (FileStream fs = File.Open(path + @"\" + "candidate_" + jsonCandidate.CandidateId + "_metainfo.json", FileMode.CreateNew))
                using (StreamWriter sw = new StreamWriter(fs))
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    jw.Formatting = Formatting.Indented;

                    JsonSerializer serializer = new JsonSerializer();

                    serializer.Serialize(jw, jsonCandidate);
                }
            }

            fi = new FileInfo(path + @"\" + "candidates-min.json");

            if (fi.Exists)
            {
                fi.Delete();
            }

            using (FileStream fs = File.Open(path + @"\" + "candidates-min.json", FileMode.CreateNew))
            using (StreamWriter sw = new StreamWriter(fs))
            using (JsonWriter jw = new JsonTextWriter(sw))
            {
                jw.Formatting = Formatting.Indented;

                JsonSerializer serializer = new JsonSerializer();

                serializer.Serialize(jw, candidateShortList);
            }
        }

        /// <summary>
        /// The add election data.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="county">
        /// The county.
        /// </param>
        /// <param name="position">
        /// The position.
        /// </param>
        /// <param name="electionYear">
        /// The election year.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<CandidateData> AddElectionData(int id, int county, int position, int electionYear)
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_AddElectionYearData", sqlConnection);
                sqlCommand.Parameters.Add(
                    new SqlParameter("@CandidateId", id));

                sqlCommand.Parameters.Add(
                    new SqlParameter("@County", county));

                sqlCommand.Parameters.Add(
                    new SqlParameter("@Position", position));

                sqlCommand.Parameters.Add(
                    new SqlParameter("@ElectionYear", electionYear));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                List<CandidateData> candidateDatas = new List<CandidateData>();
                
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        CandidateData candidateData = new CandidateData();
                        if (sqlDataReader["ElectionYear"] != DBNull.Value)
                        {
                            candidateData.ElectionYear = Convert.ToInt32(sqlDataReader["ElectionYear"]);
                        }

                        if (sqlDataReader["RepresentantId"] != DBNull.Value)
                        {
                            candidateData.CandidateId = Convert.ToInt32(sqlDataReader["RepresentantId"]);
                        }

                        if (sqlDataReader["CountyId"] != DBNull.Value)
                        {
                            candidateData.County = sqlDataReader["CountyId"].ToString();
                        }

                        candidateData.CountyName = sqlDataReader["CountyName"].ToString();

                        if (sqlDataReader["CountyId"] != DBNull.Value)
                        {
                            candidateData.Position = Convert.ToInt32(sqlDataReader["CountySeat"]);
                        }

                        candidateDatas.Add(candidateData);
                    }
                }

                return candidateDatas;
            }
        }

        /// <summary>
        /// The delete election data.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="electionYear">
        /// The election year.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List of CandidateData objects</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<CandidateData> DeleteElectionData(int id, int electionYear)
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_DeleteElectionYearData", sqlConnection);
                sqlCommand.Parameters.Add(
                    new SqlParameter("@CandidateId", id));

                sqlCommand.Parameters.Add(
                    new SqlParameter("@ElectionYear", electionYear));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                
                List<CandidateData> candidateDatas = new List<CandidateData>();
                
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        CandidateData candidateData = new CandidateData();
                        
                        if (sqlDataReader["ElectionYear"] != DBNull.Value)
                        {
                            candidateData.ElectionYear = Convert.ToInt32(sqlDataReader["ElectionYear"]);
                        }

                        if (sqlDataReader["CandidateId"] != DBNull.Value)
                        {
                            candidateData.CandidateId = Convert.ToInt32(sqlDataReader["CandidateId"]);
                        }

                        if (sqlDataReader["CountyId"] != DBNull.Value)
                        {
                            candidateData.County = sqlDataReader["CountyId"].ToString();
                        }

                        if (sqlDataReader["CountyName"] != DBNull.Value)
                        {
                            candidateData.CountyName = sqlDataReader["CountyName"].ToString();
                        }

                        if (sqlDataReader["Position"] != DBNull.Value)
                        {
                            candidateData.Position = Convert.ToInt32(sqlDataReader["Position"]);
                        }

                        candidateDatas.Add(candidateData);
                    }
                }

                return candidateDatas;
            }
        }

        /// <summary>
        /// The insert candidate.
        /// </summary>
        /// <param name="candidate">
        /// Holds the candidate object
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertCandidate(Candidate candidate)
        {
            int insertCandidateId = 0;
            try
            {
                using (
                    SqlConnection sqlConnection =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand("Valg_InsertElectionCandidate", sqlConnection);
                    sqlCommand.Parameters.Add(new SqlParameter("@FirstName", candidate.FirstName));

                    sqlCommand.Parameters.Add(new SqlParameter("@LastName", candidate.LastName));

                    sqlCommand.Parameters.Add(new SqlParameter("@BirthDate", candidate.BirthDate));

                    sqlCommand.Parameters.Add(new SqlParameter("@Gender", candidate.GenderId));

                    sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", candidate.MunicipalityId));

                    sqlCommand.Parameters.Add(new SqlParameter("@Occupation", candidate.ProfessionId));

                    sqlCommand.Parameters.Add(new SqlParameter("@Education", candidate.Education));

                    sqlCommand.Parameters.Add(new SqlParameter("@Description", candidate.Description));

                    sqlCommand.Parameters.Add(new SqlParameter("@Picture", candidate.Picture));

                    sqlCommand.Parameters.Add(new SqlParameter("@Age", candidate.Age));

                    sqlCommand.Parameters.Add(new SqlParameter("@HomeCity", candidate.HomeCity));

                    sqlCommand.Parameters.Add(new SqlParameter("@Email", candidate.Email));

                    sqlCommand.Parameters.Add(new SqlParameter("@PoliticalParty", candidate.PoliticalParty));

                    sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", candidate.ElectionYear));

                    sqlCommand.Parameters.Add(new SqlParameter("@ElectionPosition", candidate.CountyPosition));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    // Opening the connection
                    sqlConnection.Open();
                    
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            // Returning the candidateId that we get from the database
                            insertCandidateId = Convert.ToInt32(sqlDataReader["InsertId"]);

                            Logger.Info("InsertId: " + insertCandidateId);
                        }
                    }

                    candidate.CandidateId = insertCandidateId;

                    // Now that we have the insertId we could do the mapping
                    foreach (CandidateData candidateData in candidate.CandidateDatas)
                    {
                        sqlCommand = new SqlCommand("Valg_InsertCountyMap", sqlConnection);

                        sqlCommand.Parameters.Add(new SqlParameter("@CandidateId", candidate.CandidateId));
                        sqlCommand.Parameters.Add(new SqlParameter("@CountyId", candidateData.County));
                        sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", candidateData.ElectionYear));
                        sqlCommand.Parameters.Add(new SqlParameter("@Position", candidateData.Position));

                        sqlCommand.CommandType = CommandType.StoredProcedure;

                        // Executing the query
                        sqlCommand.ExecuteNonQuery();
                    }

                    // Now we can also map the election
                    sqlCommand = new SqlCommand("Valg_InsertValgMap", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@CandidateId", candidate.CandidateId));
                    sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", candidate.ElectionYear));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    // Executing the query
                    sqlCommand.ExecuteNonQuery();

                    // Inserting social media
                    if (candidate.SocialConnections != null)
                    {
                        // Now adding social connections if we have any
                        foreach (SocialConnection socialConnection in candidate.SocialConnections)
                        {
                            sqlCommand = new SqlCommand("[Valg_SetSocialConnection]", sqlConnection)
                                             {
                                                 CommandType = CommandType.StoredProcedure
                                             };

                            sqlCommand.Parameters.Add(new SqlParameter("@SocialConnectionId", socialConnection.Id));

                            sqlCommand.Parameters.Add(new SqlParameter("@RepresentantId", candidate.CandidateId));

                            sqlCommand.Parameters.Add(new SqlParameter("@Username", socialConnection.UserName));

                            // Executing the query
                            sqlCommand.ExecuteNonQuery();
                        }
                    }

                    // Now adding parliament periods
                    if (candidate.ParliamentPeriods != null)
                    {
                        foreach (ParliamentPeriod parliamentPeriod in candidate.ParliamentPeriods)
                        {
                            sqlCommand = new SqlCommand("Valg_SetParliamentPeriods", sqlConnection)
                            {
                                CommandType = CommandType.StoredProcedure
                            };

                            sqlCommand.Parameters.Add(new SqlParameter("@CandidateId", candidate.CandidateId));

                            sqlCommand.Parameters.Add(new SqlParameter("@ParliamentPeriodId", parliamentPeriod.ParliamentPeriodId));

                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }

                    return insertCandidateId;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                
                return 0;
            }
        }

        /// <summary>
        /// The populate.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="Candidate"/>.
        /// </returns>
        public Candidate Populate(FormCollection collection)
        {
            // todo: Look at this code and find out why some data is not 
            // showing up in the database or so.
            string firstName = string.Empty;

            string lastName = string.Empty;

            // If we don't have a birthdate, we set this value to null
            DateTime? birthDate = null;

            int gender = 0;

            string politicalParty = string.Empty;

            int kommune = 0;
            
            int professions = 0;

            string description = string.Empty;

            string education = string.Empty;

            string picture;

            List<SocialConnection> socialConnections = null;

            List<ParliamentPeriod> parliamentPeriods = null;

            int electionPosition = 0;

            int electionYear = 0;

            string county = string.Empty;

            int age = 0;

            string homeCity = string.Empty;

            string email = string.Empty;

            string newOccopation = string.Empty;

            // Normalizing strings and checking for values
            if (collection["Firstname"] != string.Empty)
            {
                firstName = collection["Firstname"];
                Logger.Debug(@"Firstname: " + firstName);
            }

            if (collection["Lastname"] != string.Empty)
            {
                lastName = collection["Lastname"];
                Logger.Debug(@"Lastname: " + lastName);
            }

            if (collection["Email"] != string.Empty)
            {
                email = collection["Email"];
                Logger.Debug(@"Email: " + email);
            }

            if (collection["BirthDate"] != string.Empty)
            {
                birthDate = Convert.ToDateTime(collection["BirthDate"]);
                Logger.Debug(@"Birthdate: " + birthDate);
            }

            // Age
            if (collection["Age"] != string.Empty)
            {
                age = Convert.ToInt32(collection["Age"]);
                Logger.Debug(@"Age: " + collection["Age"]);
            }

            // HomeCity
            if (collection["HomeCity"] != string.Empty)
            {
                homeCity = collection["HomeCity"];
                Logger.Debug(@"Homecity: " + homeCity);
            }

            if (collection["Gender"] != string.Empty)
            {
                gender = Convert.ToInt32(collection["Gender"]);
                Logger.Debug(@"Gender: " + gender);
            }

            if (collection["PoliticalParty"] != string.Empty)
            {
                politicalParty = collection["PoliticalParty"];
                Logger.Debug(@"Political Party: " + politicalParty);
            }

            if (collection["Kommune"] != string.Empty)
            {
                kommune = Convert.ToInt32(collection["Kommune"]);
                Logger.Debug(@"Municipality/kommune: " + kommune);
            }
            
            // Professions
            if (collection["Professions"] != string.Empty)
            {
                professions = Convert.ToInt32(collection["Professions"]);
                Logger.Debug(@"Professions: " + professions);
            }

            // Description
            if (collection["Description"] != string.Empty)
            {
                description = collection["Description"];
                Logger.Debug(@"Description: " + description);
            }

            // Education
            if (collection["Education"] != string.Empty)
            {
                education = collection["Education"];
                Logger.Debug(@"Education: " + education);
            }

            // Social media
            if (collection["sosialemedier[]"] != null || collection["sosialemedier[]"] != string.Empty)
            {
                socialConnections = ProcessSocialMediaFormInput(collection);
            }

            if (collection["parliamentperiod[]"] != null || collection["parliamentperiod[]"] != string.Empty)
            {
                parliamentPeriods = ProcessParliamentPeriodFormInput(collection);
            }

            // File
            if (collection["file"] != null)
            {
                picture = collection["file"];
                Logger.Debug(@"Picture: " + picture);
            }
            else
            {
                picture = collection["oldPicture"];
                Logger.Debug(@"Picture: " + picture);
            }

            // County
            if (collection["addCounty"] != string.Empty)
            {
                county = collection["addCounty"];
                Logger.Debug(@"County: " + county);
            }

            // ElectionYear
            if (collection["addPosition"] != string.Empty)
            {
                electionPosition = Convert.ToInt32(collection["addPosition"]);
                Logger.Debug(@"ElectionPosition: " + electionPosition);
            }

            if (collection["addElectionYear"] != string.Empty)
            {
                electionYear = Convert.ToInt32(collection["addElectionYear"]);
                Logger.Debug(@"ElectionYear: " + electionYear);
            }

            Candidate candidate = new Candidate
                {
                    FirstName = firstName,
                    LastName = lastName,
                    Age = age,
                    BirthDate = birthDate,
                    CandidateId = 0,
                    Description = description,
                    Education = education,
                    Email = email,
                    GenderId = gender,
                    HomeCity = homeCity,
                    MunicipalityId = kommune,
                    ProfessionId = professions,
                    Profession = newOccopation,
                    Picture = picture,
                    PoliticalParty = politicalParty,
                    PoliticalPartyCode = politicalParty,
                    FullName = firstName + " " + lastName,
                    County = county,
                    ElectionYear = electionYear,
                    CountyPosition = electionPosition,
                    SocialConnections = socialConnections,
                    ParliamentPeriods = parliamentPeriods
                };

            // Adding candidate Data (which is in which election they are participating in)
            CandidateData candidateData = new CandidateData
                {
                    County = county,
                    ElectionYear = electionYear,
                    Position = electionPosition
                };

            List<CandidateData> candidateDatas = new List<CandidateData> { candidateData };

            candidate.CandidateDatas = candidateDatas;

            return candidate;
        }

        /// <summary>
        /// The get total parliament members.
        /// </summary>
        /// <param name="year">
        /// The year.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Throws an exception if we don't return correct
        /// </exception>
        public int GetTotalParliamentMembers(int year)
        {
            SqlConnection sqlConnection = null;
            int totalElectionCandidates = 0;
            try
            {
                using (sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand("[Valg_GetTotalParliamentMembers]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlCommand.Parameters.Add(new SqlParameter("@Year", year));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return 0;
                    }

                    while (sqlDataReader.Read())
                    {
                        totalElectionCandidates = Convert.ToInt32(sqlDataReader["TotalParliamentMembers"]);
                    }

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }

                    return totalElectionCandidates;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                throw new Exception(
                    "Noe skjedde i forbindelse med å få ut totalt antall representanter. Be IT om å sjekke logg!");
            }
            finally
            {
                if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        /// The get total election candidates.
        /// </summary>
        /// <param name="year">
        /// The year.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetTotalElectionCandidates(int year)
        {
            SqlConnection sqlConnection = null;
            int totalElectionCandidates = 0;
            try
            {
                using (sqlConnection =
                        new SqlConnection(
                            ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand("[Valg_GetTotalElectionCandidates]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlCommand.Parameters.Add(new SqlParameter("@Year", year));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return 0;
                    }

                    while (sqlDataReader.Read())
                    {
                        totalElectionCandidates = Convert.ToInt32(sqlDataReader["TotalCandidates"]);
                    }

                    return totalElectionCandidates;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                throw new Exception(
                    "Noe skjedde i forbindelse med å få ut totalt antall representanter. Be IT om å sjekke logg!");
            }
            finally
            {
                if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }
 
        /// <summary>
        /// The process parliament period form input.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>string[]</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<ParliamentPeriod> ProcessParliamentPeriodFormInput(FormCollection collection)
        {
            try
            {
                if (collection["parliamentperiod[]"] == null)
                {
                    return null;
                }

                List<ParliamentPeriod> parliamentPeriods = new List<ParliamentPeriod>();

                if (!collection["parliamentperiod[]"].Contains(",") && collection["parliamentperiod[]"].Length > 0)
                {
                    string stringParliamentPeriod = collection["parliamentperiod[]"];

                    ParliamentPeriod parliamentPeriod = new ParliamentPeriod
                        {
                            ParliamentPeriodId = stringParliamentPeriod
                        };

                    parliamentPeriods.Add(parliamentPeriod);
                }
                else
                {
                    string[] stringParliamentPeriods = collection["parliamentperiod[]"].Split(',');

                    parliamentPeriods.AddRange(stringParliamentPeriods.Select(stringParliamentPeriod => new ParliamentPeriod { ParliamentPeriodId = stringParliamentPeriod }));
                }

                return parliamentPeriods;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);

                return null;
            }
        }

        /// <summary>
        /// The process social media form input.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<SocialConnection> ProcessSocialMediaFormInput(FormCollection collection)
        {
            try
            {
                if (collection["sosialemedier[]"] == null)
                {
                    return null;
                }
                List<SocialConnection> socialConnections = new List<SocialConnection>();

                string[] socialMediaUserName = null;
                if (!collection["sosialemedier[]"].Contains(",") && collection["sosialemedier[]"].Length > 0)
                {
                    string socialMedia = collection["sosialemedier[]"];
                    Logger.Debug("Social media: " + collection["sosialemedier[]"]);

                    int socialId = Convert.ToInt32(collection["sosialemedier[]"]);

                    if (collection["sm_username[]"] != null)
                    {
                        if (collection["sm_username[]"] != null)
                        {
                            string usernames = collection["sm_username[]"];
                            usernames = usernames.Replace(",,", ",");
                            socialMediaUserName = usernames.Split(',');
                        }

                        socialConnections = new List<SocialConnection>();

                        SocialConnection socialConnection = new SocialConnection();

                        if (socialMediaUserName != null)
                        {
                            socialConnection.UserName = socialMediaUserName[1];
                        }

                        if (socialMedia != null)
                        {
                            socialConnection.Name = socialMedia;
                        }

                        socialConnection.Id = socialId;
                        
                        socialConnections.Add(socialConnection);

                        return socialConnections;
                    }
                }

                if (collection["sosialemedier[]"].Contains(","))
                {
                    string[] socialMedia = collection["sosialemedier[]"].Split(',');

                    if (collection["sm_username[]"] != null)
                    {
                        string usernames = collection["sm_username[]"];
                        usernames = usernames.Replace(",,", ",");
                        socialMediaUserName = usernames.Split(',');
                    }

                    if (socialMedia.Any())
                    {
                        socialConnections = new List<SocialConnection>();
                        int counter = 0;
                        foreach (var socialId in socialMedia)
                        {
                            if (socialMediaUserName != null)
                            {
                                SocialConnection socialConnection = new SocialConnection
                                                                          {
                                                                              Name = socialMedia[counter],
                                                                              Id = Convert.ToInt32(socialId),
                                                                              UserName = socialMediaUserName[counter]
                                                                          };
                                socialConnections.Add(socialConnection);
                            }

                            counter++;
                        }
                    }
                }
                return socialConnections;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return null;
            }
        }
    }
}