﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValgModels.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the ValgModels type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Models
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Web.Mvc;

    using ElectionCandidates.Classes;

    /// <summary>
    /// The valg models.
    /// </summary>
    public class ElectionModels
    {
        /// <summary>
        /// The get elections.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public static List<Election> GetElections()
        {
            using (SqlConnection sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetValg", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlConnection.Open();

                List<Election> elections = new List<Election>();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            int electionYear = 0;
                            if (sqlDataReader["ElectionYear"] != DBNull.Value)
                            {
                                electionYear = Convert.ToInt32(sqlDataReader["ElectionYear"]);
                            }

                            int electionTypeId = 0;
                            if (sqlDataReader["ElectionTypeId"] != DBNull.Value)
                            {
                                electionTypeId = Convert.ToInt32(sqlDataReader["ElectionTypeId"]);
                            }

                            string electionType = sqlDataReader["ElectionType"].ToString();

                            string electionName = sqlDataReader["ElectionName"].ToString();

                            Election election = new Election
                                {
                                    ElectionYear = electionYear,
                                    ElectionTypeId = electionTypeId,
                                    ElectionType = electionType,
                                    ElectionName = electionName
                                };

                            elections.Add(election);
                        }
                    }

                    return elections;
                }
            }
        }

        /// <summary>
        /// The get mapped political parties.
        /// </summary>
        /// <param name="electionYear">
        /// The election year.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public static List<Parti> GetMappedPoliticalParties(int electionYear)
        {
            using (
                SqlConnection sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetPartiesForSelectedElection", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", electionYear));

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                List<Parti> politicalparties = new List<Parti>();

                if (sqlDataReader.HasRows)
                {

                    while (sqlDataReader.Read())
                    {
                        Parti parti = new Parti();
                        parti.Code = sqlDataReader["PartiKode"].ToString();
                        parti.Name = sqlDataReader["PartiNavn"].ToString();

                        politicalparties.Add(parti);
                    }
                }

                // Returning the list
                return politicalparties;
            }
        }

        /// <summary>
        /// The connect parties to election.
        /// </summary>
        /// <param name="electionYear">
        /// The election year.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        public void ConnectPartiesToElection(int electionYear, FormCollection collection)
        {
            using (SqlConnection sqlConnection = new SqlConnection(
                        ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString)) 
            {
                // First we empty the table
                SqlCommand sqlCommand = new SqlCommand("Valg_DeletePoliticalPartyFromElection", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", electionYear));

                sqlConnection.Open();

                sqlCommand.ExecuteNonQuery();

                sqlConnection.Close();

                // Now we shall insert
                if (collection["party[]"] == null)
                {
                    return;
                }

                string[] politicalParties = collection["party[]"].Split(',');

                sqlConnection.Open();

                foreach (string politicalParty in politicalParties)
                {
                    sqlCommand = new SqlCommand("[Valg_MapPoliticalPartyToElection]", sqlConnection);
                    sqlCommand.Parameters.Add(new SqlParameter("@ElectionYear", electionYear));
                    sqlCommand.Parameters.Add(new SqlParameter("@PoliticalPartyCode", politicalParty));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.ExecuteNonQuery();
                }
            }
        }
    }
}