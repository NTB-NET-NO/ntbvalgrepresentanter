// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CandidateText.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The candidate text.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The candidate text.
    /// </summary>
    public class CandidateText
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the education.
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// Gets or sets the occupation.
        /// </summary>
        public int Occupation { get; set; }
    }
}