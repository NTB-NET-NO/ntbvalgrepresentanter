﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Kommune.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The kommune.
    /// </summary>
    public class Kommune
    {
        /// <summary>
        /// Gets or sets the kommune id.
        /// </summary>
        public int KommuneId { get; set; }

        /// <summary>
        /// Gets or sets the krets id.
        /// </summary>
        public int KretsId { get; set; }

        /// <summary>
        /// Gets or sets the kommune navn.
        /// </summary>
        public string KommuneNavn { get; set; }
    }
}