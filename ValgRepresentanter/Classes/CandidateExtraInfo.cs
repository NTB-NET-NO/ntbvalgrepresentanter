// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CandidateExtraInfo.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The candidate extra info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The candidate extra info.
    /// </summary>
    public class CandidateExtraInfo
    {
        /// <summary>
        /// Gets or sets the gender id.
        /// </summary>
        public int GenderId { get; set; }

        /// <summary>
        /// Gets or sets the gender name.
        /// </summary>
        public string GenderName { get; set; }

        /// <summary>
        /// Gets or sets the age.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }
    }
}