﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Parti.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The parti.
    /// </summary>
    public class Parti
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the parti id.
        /// </summary>
        public int PartiId { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the sorting.
        /// </summary>
        public int SortOrder { get; set; }
    }
}