﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Gender.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The gender.
    /// </summary>
    public class Gender
    {
        /// <summary>
        /// Gets or sets the gender id.
        /// </summary>
        public int GenderId { get; set; }

        /// <summary>
        /// Gets or sets the gender name.
        /// </summary>
        public string GenderName { get; set; }
    }
}