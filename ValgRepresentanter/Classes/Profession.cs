﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Profession.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The profession.
    /// </summary>
    public class Profession
    {
        /// <summary>
        /// Gets or sets the profession id.
        /// </summary>
        public int ProfessionId { get; set; }

        /// <summary>
        /// Gets or sets the profession name.
        /// </summary>
        public string ProfessionName { get; set; }
    }
}