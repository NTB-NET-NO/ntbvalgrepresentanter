﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CandidateData.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the CandidateData type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The candidate data.
    /// </summary>
    public class CandidateData
    {
        /// <summary>
        /// Gets or sets the candidate id.
        /// </summary>
        public int CandidateId { get; set; }

        /// <summary>
        /// Gets or sets the county.
        /// </summary>
        public string County { get; set; }

        /// <summary>
        /// Gets or sets the county name.
        /// </summary>
        public string CountyName { get; set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Gets or sets the election year.
        /// </summary>
        public int ElectionYear { get; set; }
    }
}