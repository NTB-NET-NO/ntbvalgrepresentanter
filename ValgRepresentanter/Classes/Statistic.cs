﻿namespace ElectionCandidates.Classes
{
    public class Statistic
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }
}