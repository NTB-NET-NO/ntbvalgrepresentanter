﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="User.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The user.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the pass word.
        /// </summary>
        public string PassWord { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether validated.
        /// </summary>
        public bool Validated { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the client name.
        /// </summary>
        public string ClientName { get; set; }
    }
}