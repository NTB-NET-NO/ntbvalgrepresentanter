// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CandidateMetaInfo.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The candidate meta info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    using System.Collections.Generic;

    using ElectionCandidates.Models;

    /// <summary>
    /// The candidate meta info.
    /// </summary>
    public class CandidateMetaInfo
    {
        /// <summary>
        /// Gets or sets the candidate datas.
        /// </summary>
        public List<CandidateData> CandidateDatas { get; set; }

        /// <summary>
        /// Gets or sets the county representants.
        /// </summary>
        public List<CountyRepresentant> CountyRepresentants { get; set; }

        /// <summary>
        /// Gets or sets the social connections.
        /// </summary>
        public List<SocialConnection> SocialConnections { get; set; }

        /// <summary>
        /// Gets or sets the candidate text.
        /// </summary>
        public CandidateText CandidateText { get; set; }

        /// <summary>
        /// Gets or sets the candidate extra info.
        /// </summary>
        public CandidateExtraInfo CandidateExtraInfo { get; set; }
    }
}