﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SocialConnections.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The social connection.
    /// </summary>
    public class SocialConnection
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// This can for instance be the e-mail address
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// This can for instance be e-mail
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the form field name.
        /// this is what we use as the label for the socialConnection
        /// </summary>
        public string FormFieldName { get; set; }
    }
}