﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="County.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The county.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The county.
    /// </summary>
    public class County
    {
        /// <summary>
        /// Gets or sets the county id.
        /// </summary>
        public int CountyId { get; set; }

        /// <summary>
        /// Gets or sets the county name.
        /// </summary>
        public string CountyName { get; set; }
    }
}