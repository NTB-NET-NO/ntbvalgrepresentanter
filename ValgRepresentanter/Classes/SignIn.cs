﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SignIn.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// The sign in.
    /// </summary>
    public class SignIn
    {
        /// <summary>
        /// Constructor for the SignIn class
        /// </summary>
        /// <param name="username">
        /// The Username.
        /// </param>
        /// <param name="password">
        /// The Password.
        /// </param>
        /// <returns>
        /// The <see cref="User"/>.
        /// </returns>
        public User UserValidated(string username, string password)
        {
            // If no username has been provided we return false
            if (username == string.Empty)
            {
                return null;
            }

            // If no password has been provided we return false
            if (password == string.Empty)
            {
                return null;
            }

            using (
                SqlConnection sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["ValgDataConnectionString"].ConnectionString))
            {
                SqlCommand sqlCommand = new SqlCommand("Valg_GetUserIdAndClientName", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@Username", username));
                sqlCommand.Parameters.Add(new SqlParameter("@Password", password));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                sqlConnection.Open();

                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            int userId = 0;
                            
                            if (sqlDataReader["iClientId"] != DBNull.Value)
                            {
                                userId = Convert.ToInt32(sqlDataReader["iClientId"]);
                            }

                            string clientName = sqlDataReader["sClientName"].ToString();

                            User user = new User
                                            {
                                                UserName = username,
                                                PassWord = password,
                                                UserId = userId,
                                                ClientName = clientName,
                                                Validated = true
                                            };

                            return user;
                        }
                    }
                }
            }

            return null;
        }
    }
}