// --------------------------------------------------------------------------------------------------------------------
// <copyright file="File.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    
    using Controllers;

    using log4net;

    /// <summary>
    /// The file.
    /// </summary>
    public class File
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(File));

        /// <summary>
        /// The _candidate controller.
        /// </summary>
        private readonly CandidateController _candidateController;

        /// <summary>
        /// Initializes a new instance of the <see cref="File"/> class.
        /// </summary>
        public File()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="File"/> class.
        /// </summary>
        /// <param name="candidateController">
        /// The candidate controller.
        /// </param>
        public File(CandidateController candidateController)
        {
            _candidateController = candidateController;
        }

        /// <summary>
        /// The upload.
        /// </summary>
        /// <param name="fullName">
        /// The full Name.
        /// </param>
        /// <param name="politicalPartyCode">
        /// The political Party Code.
        /// </param>
        /// <param name="county">
        /// The county.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string Upload(string fullName, string politicalPartyCode, string county)
        {
            try
            {
                string filename = string.Empty;

                var fileBase = _candidateController.Request.Files["pic"];
                Logger.Debug("fileBase: " + fileBase);
                if (fileBase != null && (_candidateController.Request.Files.Count > 0 && fileBase.FileName.Length > 0))
                {
                    filename = CreateFilename(fullName, politicalPartyCode, county);
                    HttpPostedFileBase file = _candidateController.Request.Files[0];

                    Logger.Debug("Political PartyCode: " + politicalPartyCode);
                    Logger.Debug("county: " + county);
                    Logger.Debug("Server.MapPath: " + _candidateController.Server.MapPath("~/Content/uploads/"));

                    string directoryName = CreateDirectoryName(politicalPartyCode, county);
                    string path = CreateDirectory(_candidateController.Server.MapPath("~/Content/uploads/"), directoryName);

                    if (file != null)
                    {
                        Logger.Info(
                            @"Calling resize: with parameters: file:" + file.ContentType + @", path: " + path
                            + @", filename:" + filename);
                        filename = Resize(file, path, filename);
                    }
                    Logger.Debug("We are returning: " + filename);
                    return filename;
                }

                Logger.Debug("We are returning: " + filename);
                return filename;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                throw new Exception("Could not upload picture!");
            }
        }

        /// <summary>
        /// The generate filename.
        /// </summary>
        /// <param name="fullName">
        /// The full Name.
        /// </param>
        /// <param name="politicalPartyCode">
        /// The political Party Code.
        /// </param>
        /// <param name="county">
        /// The county.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string CreateFilename(string fullName, string politicalPartyCode, string county)
        {
            fullName = fullName.Replace(" ", string.Empty);
            fullName = ChangeForeignCharacters(fullName);

            int c = Convert.ToInt32(county);
            county = c.ToString("D" + 2);
            Logger.Debug("Created Filename is: " + politicalPartyCode + "_" + county + "_" + fullName);
            return politicalPartyCode + "_" + county + "_" + fullName;
        }

        /// <summary>
        /// The change foreign characters.
        /// </summary>
        /// <param name="str">
        /// The str.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string ChangeForeignCharacters(string str)
        {
            var foreignChar = new[]
                                  {
                                      '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
                                      '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
                                      '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
                                      '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�'
                                  };
            var replaceChar = new[]
                                  {
                                      "ae", "oe", "aa", "a", "e", "i", "oe", "u", "y", "a", "e", "i", "o", "u", "y", "a",
                                      "e", "i", "o", "u", "y", "a", "e", "i", "o", "u", "AE", "OE", "AA", "A", "E", "I",
                                      "OE", "U", "A", "E", "I", "O", "U", "A", "E", "I", "O", "U", "Y", "A", "E", "I", "O", "U"
                                  };

            StringBuilder sb = new StringBuilder();

            foreach (char c in str)
            {
                if (foreignChar.Contains(c))
                {
                    int index = Array.IndexOf(foreignChar, c);

                    sb.Append(replaceChar[index]);
                }
                else
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="politicalParty">
        /// The political Party.
        /// </param>
        /// <param name="county">
        /// The county.
        /// </param>
        /// <param name="mapPath">
        /// The map path.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string CreateDirectoryName(string politicalParty, string county)
        {
            if (county.Length < 2)
            {
                int c = Convert.ToInt32(county);
                county = c.ToString("D" + 2);
            }

            return @"\" + politicalParty + @"\" + county + @"\";
        }
        /// <summary>
        /// The create directory.
        /// </summary>
        /// <param name="directoryName">
        /// The name of the directory.
        /// </param>
        /// <param name="mapPath">
        /// The map path.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string CreateDirectory(string mapPath, string directoryName)
        {

            mapPath += @"\" + directoryName;
            Logger.Debug("This is directory: " + mapPath);

            // Checking if the directory exists, and if not, then create it
            DirectoryInfo directoryInfo = new DirectoryInfo(mapPath);
            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }

            // Return the map-path for tihs picture
            return mapPath;
        }

        /// <summary>
        /// The resize.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="fileName">
        /// The filename.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string Resize(HttpPostedFileBase file, string path, string fileName)
        {
            Logger.Debug("file: " + file);
            Logger.Debug("file.ContentLength: " + file.ContentLength);
            Logger.Debug("file.ContentType: " + file.ContentType);
            Logger.Debug("file.FileName: " + file.FileName);
            Logger.Debug("file.InputStream: " + file.InputStream);
            try
            {
                List<ImageSizes> imageSizeses = GetImageSizes();

                string filetype = Path.GetExtension(file.FileName);
                Logger.Debug("Filetype: " + filetype);

                string baseName = path + fileName;
                string orgFile = path + fileName + "_org" + filetype;
                fileName = fileName + "_org" + filetype;

                // Now we are storing the file
                
                FileInfo fileInfo = new FileInfo(orgFile);

                // Deleting file if it exists
                if (fileInfo.Exists)
                {
                    fileInfo.Delete();
                }
                file.SaveAs(orgFile);
                

                // Creating an image that we can resize from
                Image image = Image.FromFile(orgFile);

                /**
                 * Consider adding a test saying that we shall resize picture or not...
                 * (Just that we need it now, because we need one picture on the front)
                 * 
                 */

                foreach (ImageSizes sizes in imageSizeses)
                {
                    try
                    {
                        double maxHeight = sizes.Height;
                        double maxWidth = sizes.Width;
                        double imageWidth = image.Width;
                        double imageHeight = image.Height;

                        double aspectRatio = imageWidth / imageHeight;
                        double boxRatio = maxWidth / maxHeight;
                        double scaleFactor;

                        if (boxRatio > aspectRatio)
                        {
                            // Use height, since that is the most restrictive dimension of box. 
                            scaleFactor = maxHeight / imageHeight;
                        }
                        else
                        {
                            scaleFactor = maxWidth / imageWidth;
                        }

                        // Calculating the new width and height
                        int newWidth = (int)(image.Width * scaleFactor);
                        int newHeight = (int)(image.Height * scaleFactor);

                        // Creating the new bitmap (We could need a few more here)
                        Bitmap thumbnailBitmap = new Bitmap(newWidth, newHeight);

                        Graphics thumbnailGraphics = Graphics.FromImage(thumbnailBitmap);
                        thumbnailGraphics.CompositingQuality = CompositingQuality.HighQuality;
                        thumbnailGraphics.SmoothingMode = SmoothingMode.HighQuality;
                        thumbnailGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                        Rectangle imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                        thumbnailGraphics.DrawImage(image, imageRectangle);

                        string newFilename = baseName + "_" + sizes.Width + "x" + sizes.Height + filetype;

                        fileInfo = new FileInfo(newFilename);

                        if (fileInfo.Exists)
                        {
                            fileInfo.Delete();
                        }

                        thumbnailBitmap.Save(newFilename, image.RawFormat);

                        thumbnailGraphics.Dispose();
                        thumbnailBitmap.Dispose();
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);
                    }
                }

                image.Dispose();

                return fileName;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                throw new Exception("Could not resize image!");
            }
        }

        /// <summary>
        /// The image sizes.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<ImageSizes> GetImageSizes()
        {
            /*
             * Flickr Image Sizes
             * Square 75 x 75
             * Thumbnail 100 x 67
             * Small 240 x 161
             * Medium 500 x 334
             * Large 1024 x 768

             * Picassa Image Sizes
             * 
             * Picassa Web Image Sizes
             * Medium 640 x 428
             * Large Thumbnail 160 x 160
             * Thumbnail 144 x 96
             */
            List<ImageSizes> imageSizeses = new List<ImageSizes>
                {
                    new ImageSizes
                        {
                            Width = 75, 
                            Height = 75, 
                            Name = "Square"
                        }, 
                    new ImageSizes
                        {
                            Width = 75, 
                            Height = 100, 
                            Name = "Thumbnail"
                        }, 
                    new ImageSizes
                        {
                            Width = 161, 
                            Height = 240, 
                            Name = "Small"
                        }, 
                    new ImageSizes
                        {
                            Width = 334, 
                            Height = 500, 
                            Name = "Medium"
                        }
                };

            return imageSizeses;
        }
    }
}