﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Client.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the Client type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The client.
    /// </summary>
    public class Client
    {
        /// <summary>
        /// Gets or sets the client id.
        /// </summary>
        public int ClientId { get; set; }

        /// <summary>
        /// Gets or sets the client name.
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        /// Gets or sets the client url.
        /// </summary>
        public string ClientUrl { get; set; }

        /// <summary>
        /// Gets or sets the client contact.
        /// </summary>
        public string ClientContact { get; set; }

        /// <summary>
        /// Gets or sets the client contact email.
        /// </summary>
        public string ClientContactEmail { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the pass word.
        /// </summary>
        public string PassWord { get; set; }

        /// <summary>
        /// Gets or sets the publish method.
        /// </summary>
        public int PublishMethod { get; set; }

        /// <summary>
        /// Gets or sets the caption.
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Gets or sets the css.
        /// </summary>
        public string CSS { get; set; }

        /// <summary>
        /// Gets or sets the show district.
        /// </summary>
        public int ShowDistrict { get; set; }
    }
}