// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Election.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the Election type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The election.
    /// </summary>
    public class Election
    {
        /// <summary>
        /// Gets or sets the election year.
        /// </summary>
        public int ElectionYear { get; set; }

        /// <summary>
        /// Gets or sets the election type id.
        /// </summary>
        public int ElectionTypeId { get; set; }

        /// <summary>
        /// Gets or sets the election type.
        /// </summary>
        public string ElectionType { get; set; }

        /// <summary>
        /// Gets or sets the election name.
        /// </summary>
        public string ElectionName { get; set; }
    }
}
