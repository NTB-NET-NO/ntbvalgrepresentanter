﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ParliamentPeriod.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    using System;

    /// <summary>
    /// The parliament period.
    /// </summary>
    public class ParliamentPeriod
    {
        /// <summary>
        /// Gets or sets the date from.
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// Gets or sets the date to.
        /// </summary>
        public DateTime DateTo { get; set; }

        /// <summary>
        /// Gets or sets the election period id.
        /// </summary>
        public string ParliamentPeriodId { get; set; }
    }
}