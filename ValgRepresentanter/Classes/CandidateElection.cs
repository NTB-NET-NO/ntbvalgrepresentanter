// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CandidateElection.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The candidate election.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The candidate election.
    /// </summary>
    public class CandidateElection
    {
        /// <summary>
        /// Gets or sets the election year.
        /// </summary>
        public int ElectionYear { get; set; }

        /// <summary>
        /// Gets or sets the election text.
        /// </summary>
        public string ElectionText { get; set; }

        /// <summary>
        /// Gets or sets the election type name.
        /// </summary>
        public string ElectionTypeName { get; set; }
    }
}