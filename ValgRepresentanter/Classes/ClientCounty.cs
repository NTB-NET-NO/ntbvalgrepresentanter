﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClientCounty.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The client county.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The client county.
    /// </summary>
    public class ClientCounty
    {
        /// <summary>
        /// Gets or sets the client county id.
        /// </summary>
        public int ClientCountyId { get; set; }

        /// <summary>
        /// Gets or sets the client county name.
        /// </summary>
        public string ClientCountyName { get; set; }
    }
}