// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CandidateShort.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace ElectionCandidates.Classes
{
    using System.Collections.Generic;

    /// <summary>
    /// The candidate short.
    /// </summary>
    public class CandidateShort
    {
        /// <summary>
        /// Gets or sets the candidate id.
        /// </summary>
        public int CandidateId { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the picture.
        /// </summary>
        public string Picture { get; set; }

        /// <summary>
        /// Gets or sets the political party code.
        /// </summary>
        public string PoliticalPartyCode { get; set; }

        /// <summary>
        /// Gets or sets the county id.
        /// </summary>
        public int CountyId { get; set; }

        /// <summary>
        /// Gets or sets the county name.
        /// </summary>
        public string CountyName { get; set; }

        /// <summary>
        /// Gets or sets the county position.
        /// </summary>
        public int CountyPosition { get; set; }

        /// <summary>
        /// Gets or sets the election year.
        /// </summary>
        public int ElectionYear { get; set; }

        /// <summary>
        /// Gets or sets the parliament periods.
        /// </summary>
        public List<ParliamentPeriod> ParliamentPeriods { get; set; }

        /// <summary>
        /// Gets or sets the parti sort order.
        /// </summary>
        public int PartiSortOrder { get; set; }

        /// <summary>
        /// Gets or sets the parti id.
        /// </summary>
        public int PartiId { get; set; }

        /// <summary>
        /// Gets or sets the parti organization id.
        /// </summary>
        public int PartiOrganizationId { get; set; }

        /// <summary>
        /// Gets or sets the home town.
        /// </summary>
        public string HomeCity { get; set; }

        /// <summary>
        /// Gets or sets the municipality.
        /// </summary>
        public string Municipality { get; set; }
    }
}