﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CandidateLong.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace ElectionCandidates.Classes
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// The candidate long.
    /// </summary>
    public class CandidateLong
    {
        /// <summary>
        /// Gets or sets the candidate id.
        /// </summary>
        public int CandidateId { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the date of birth
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the picture.
        /// </summary>
        public string Picture { get; set; }

        /// <summary>
        /// Gets or sets the county id.
        /// </summary>
        public int CountyId { get; set; }

        /// <summary>
        /// Gets or sets the county name.
        /// </summary>
        public string CountyName { get; set; }

        /// <summary>
        /// Gets or sets the county position.
        /// </summary>
        public int CountyPosition { get; set; }

        /// <summary>
        /// Gets or sets the election year.
        /// </summary>
        public int ElectionYear { get; set; }

        /// <summary>
        /// Gets or sets the political party code.
        /// </summary>
        public string PoliticalPartyCode { get; set; }

        /// <summary>
        /// Gets or sets the parti sort order.
        /// </summary>
        public int PartiSortOrder { get; set; }

        /// <summary>
        /// Gets or sets the parti id.
        /// </summary>
        public int PartiId { get; set; }

        /// <summary>
        /// Gets or sets the parti organization id.
        /// </summary>
        public int PartiOrganizationId { get; set; }
        
        /// <summary>
        /// Gets or sets the birthtown
        /// </summary>
        public string BirthTown { get; set; }

        /// <summary>
        /// Gets or sets the hometown
        /// </summary>
        public string HomeTown { get; set; }
        
        /// <summary>
        /// Gets or sets the gender id.
        /// </summary>
        public int GenderId { get; set; }

        /// <summary>
        /// Gets or sets the gender name.
        /// </summary>
        public string GenderName { get; set; }

        /// <summary>
        /// Gets or sets the age.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the social connections.
        /// </summary>
        public List<SocialConnection> SocialConnections { get; set; }

        /// <summary>
        /// Gets or sets the candidate text.
        /// </summary>
        public string CandidateText { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the education.
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// Gets or sets the occupation.
        /// </summary>
        public string Occupation { get; set; }

        /// <summary>
        /// Gets or sets the parliament periods.
        /// </summary>
        public List<ParliamentPeriod> ParliamentPeriods { get; set; }

        /// <summary>
        /// Gets or sets the municipality.
        /// </summary>
        public string Municipality { get; set; }
    }
}