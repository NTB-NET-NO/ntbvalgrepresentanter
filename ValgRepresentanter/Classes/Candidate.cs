﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Candidate.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the Candidate type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The candidate.
    /// </summary>
    public class Candidate
    {
        /// <summary>
        /// Gets or sets the candidate id.
        /// </summary>
        public int CandidateId { get; set; }

        /// <summary>
        /// Gets or sets the election periods.
        /// </summary>
        public int ElectionYear { get; set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the political party code.
        /// </summary>
        public string PoliticalPartyCode { get; set; }

        /// <summary>
        /// Gets or sets the political party.
        /// </summary>
        public string PoliticalParty { get; set; }

        /// <summary>
        /// Gets or sets the gender id.
        /// </summary>
        public int GenderId { get; set; }

        /// <summary>
        /// Gets or sets the gender name.
        /// </summary>
        public string GenderName { get; set; }

        /// <summary>
        /// Gets or sets the birth date.
        /// </summary>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Gets or sets the age.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets the home city.
        /// </summary>
        public string HomeCity { get; set; }

        /// <summary>
        /// Gets or sets the municipality id.
        /// </summary>
        public int MunicipalityId { get; set; }
        
        /// <summary>
        /// Gets or sets the profession.
        /// </summary>
        public string Profession { get; set; }

        /// <summary>
        /// Gets or sets the profession id.
        /// </summary>
        public int ProfessionId { get; set; }

        /// <summary>
        /// Gets or sets the education.
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the picture.
        /// </summary>
        public string Picture { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the social connections.
        /// </summary>
        public List<SocialConnection> SocialConnections { get; set; }

        /// <summary>
        /// Gets or sets the counties.
        /// </summary>
        public List<CountyRepresentant> Counties { get; set; }

        /// <summary>
        /// Gets or sets the name of the county.
        /// </summary>
        public string County { get; set; }

        /// <summary>
        /// Gets or sets the position in the current election
        /// </summary>
        public int CountyPosition { get; set; }

        /// <summary>
        /// Gets or sets the candidate datas.
        /// </summary>
        public List<CandidateData> CandidateDatas { get; set; }

        /// <summary>
        /// Gets or sets the parliament periods.
        /// </summary>
        public List<ParliamentPeriod> ParliamentPeriods { get; set; }

        /// <summary>
        /// Gets or sets the municipality.
        /// </summary>
        public string Municipality { get; set; }
    }
}