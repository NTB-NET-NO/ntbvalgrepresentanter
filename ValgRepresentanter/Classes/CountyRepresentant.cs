﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CountyRepresentant.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Classes
{
    /// <summary>
    /// The county representant.
    /// </summary>
    public class CountyRepresentant
    {
        /// <summary>
        /// Gets or sets the county id.
        /// </summary>
        public int CountyId { get; set; }

        /// <summary>
        /// Gets or sets the representant id.
        /// </summary>
        public int RepresentantId { get; set; }

        /// <summary>
        /// Gets or sets the county position.
        /// </summary>
        public int CountyPosition { get; set; }

        /// <summary>
        /// Gets or sets the county name.
        /// </summary>
        public string CountyName { get; set; }

        /// <summary>
        /// Gets or sets the election year.
        /// </summary>
        public int? ElectionYear { get; set; }
    }
}