﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ToolsController.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The tools controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    using ElectionCandidates.Classes;
    using ElectionCandidates.Models;
    using ElectionCandidates.Models.DataModels;

    /// <summary>
    /// The tools controller.
    /// </summary>
    public class ToolsController : Controller
    {
        /// <summary>
        ///     The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        ///     The add parliament periods.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult AddParliamentPeriods()
        {
            return View();
        }

        /// <summary>
        /// The get parliament periods.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GetParliamentPeriods()
        {
            ToolsModels toolsModel = new ToolsModels();
            List<ParliamentPeriod> parliamentPeriods = toolsModel.GetParliamentPeriods();

            ParliamentDataModel parliamentDataModel = new ParliamentDataModel();
            parliamentDataModel.InsertParliamentPeriods(parliamentPeriods);

            return View("Index");
        }

        /// <summary>
        /// The check current candidates.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult CheckCurrentCandidates()
        {
            ToolsModels toolsModel = new ToolsModels();
            List<Candidate> currentCandidates = toolsModel.GetCurrentParliamentMembersFromParliament();
            Session["CurrentCandidates"] = currentCandidates;
            CandidatesModels candidatesModels = new CandidatesModels();
            List<Candidate> candidates = candidatesModels.GetRepresentanter(DateTime.Today.Year);
            candidates = toolsModel.MatchCurrentParliamentAgainstCandidateList(candidates, currentCandidates);
            return this.View("CheckCurrentCandidates", candidates);
        }

        /// <summary>
        /// The update checked current candidates.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult UpdateCheckedCurrentCandidates()
        {
            ToolsModels toolsModel = new ToolsModels();
            List<Candidate> currentCandidates = toolsModel.GetCurrentParliamentMembersFromParliament();
            CandidatesModels candidatesModels = new CandidatesModels();
            List<Candidate> candidates = candidatesModels.GetRepresentanter(DateTime.Today.Year);
            candidates = toolsModel.MatchCurrentParliamentAgainstCandidateList(candidates, currentCandidates);

            if (candidates != null)
            {
                toolsModel.UpdateCandidates(candidates);
            }

            return this.View("Index");
        }

        /// <summary>
        /// The check candidate picture.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult CheckCandidatePicture()
        {
            CandidatesModels candidatesModels = new CandidatesModels();
            List<Candidate> candidates = candidatesModels.GetRepresentanter(DateTime.Today.Year);
            
            ToolsModels toolsModel = new ToolsModels();
            List<Candidate> correctedCandidates = toolsModel.CheckCandidatesPicture(candidates);

            toolsModel.UpdateCandidates(correctedCandidates);

            return this.View("CheckCandidatePicture", correctedCandidates);
        }

        /// <summary>
        /// The check candidate picture.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult CheckAndUploadCandidatePicture()
        {
            CandidatesModels candidatesModels = new CandidatesModels();
            List<Candidate> candidates = candidatesModels.GetRepresentanter(DateTime.Today.Year);

            return this.View("CheckAndUploadCandidatePicture", candidates);
        }

        public ActionResult GetCurrentParliamentMembers()
        {
            ToolsModels toolsModel = new ToolsModels();
            List<Candidate> currentCandidates = toolsModel.GetCurrentParliamentMembersFromParliament();

            return this.View("GetCurrentParliamentMembers", currentCandidates);
        }
    }
}
