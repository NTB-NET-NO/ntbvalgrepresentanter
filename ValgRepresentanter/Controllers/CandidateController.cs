﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CandidateController.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the CandidateController type.
// </summary>
// todo: Fix the number of objects we have created for candidates
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Controllers
{
    using System;
    using System.Web.Mvc;

    using ElectionCandidates.Classes;
    using ElectionCandidates.Models;

    using log4net;

    using File = ElectionCandidates.Classes.File;

    /// <summary>
    /// The candidate controller.
    /// </summary>
    public class CandidateController : Controller
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(CandidateController));

        /// <summary>
        /// The _file.
        /// </summary>
        private readonly File _file;

        /// <summary>
        /// Initializes a new instance of the <see cref="CandidateController"/> class.
        /// </summary>
        public CandidateController()
        {
            _file = new File(this);

            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// The generate data file is called like this: GET: /Candidate/.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult GenerateDataFile()
        {
            CandidatesModels candidatesModels = new CandidatesModels();
            int electionYear = Convert.ToInt32(Session["Year"]);
            candidatesModels.GenerateDataFile(electionYear);

            return RedirectToAction("Index");
        }

        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ViewResult"/>.
        /// </returns>
        public ViewResult Index()
        {
            // CandidatesModels repModels = new CandidatesModels();

            // return View(repModels.GetRepresentanter());
            try
            {
                return View(ElectionModels.GetElections());
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }

        /// <summary>
        /// The details.
        /// </summary>
        /// <param name="id">
        /// The id is called like this GET: /Candidate/Details/5
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Details(int id)
        {
            try
            {
                CandidatesModels candidatesModels = new CandidatesModels();
                Candidate candidate = candidatesModels.GetRepresentant(id);

                Logger.Debug("PartyCode: " + candidate.PoliticalPartyCode);
                Logger.Debug("County: " + candidate.County);
                Logger.Debug("Picture: " + candidate.Picture);

                return View(candidate);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }

        /// <summary>
        /// The list of elections.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>list of elections</returns>
        public ActionResult List()
        {
            try
            {
                return View("Index", ElectionModels.GetElections());
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return this.View("Error");
            }
        }

        /// <summary>
        /// The list candidates.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ListCandidatesWithoutId()
        {
            try
            {
                if (Session["Year"] != null)
                {
                    CandidatesModels candidatesModels = new CandidatesModels();
                    return View("ListCandidates", candidatesModels.GetRepresentanter(Convert.ToInt32(Session["Year"])));
                }
                else
                {
                    return RedirectToAction("Home");
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }

        /// <summary>
        /// The list of election candidates.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult ListCandidates(string id)
        {
            try
            {
                if (Convert.ToInt32(id) < 1000)
                {
                    return RedirectToAction("ListCandidatesWithoutId");
                }
                
                Session["Year"] = Convert.ToInt32(id);
                
                CandidatesModels candidatesModels = new CandidatesModels();
                ViewBag.TotalParliamentMembers = candidatesModels.GetTotalParliamentMembers(Convert.ToInt32(Session["Year"].ToString()));
                ViewBag.TotalCandidates = candidatesModels.GetTotalElectionCandidates(Convert.ToInt32(Session["Year"].ToString()));

                return this.View(candidatesModels.GetRepresentanter(Convert.ToInt32(id)));
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }

        /// <summary>
        /// The create is called like this: GET: /Candidate/Create
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Create()
        {
            try
            {
                return View(); // <- the original line
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }

        /// <summary>
        /// The create is called like this: POST: /Candidate/Create
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                CandidatesModels candidatesModels = new CandidatesModels();

                Candidate candidate = candidatesModels.Populate(collection);

                var filename = _file.Upload(candidate.FullName, candidate.PoliticalPartyCode, candidate.County);

                candidate.Picture = filename;

                candidate.ElectionYear = (int)Session["Year"];

                candidatesModels.InsertCandidate(candidate);
                
                return RedirectToAction("ListCandidates");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }
        
        /// <summary>
        /// The edit is called like this: GET: /Candidate/Edit/5
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Edit(int id)
        {
            try
            {
                // So we have to get the values from the Representanter Object
                // But we also need the values for Gender / Parti / Social Connections and so on
                CandidatesModels candidatesModels = new CandidatesModels();

                CandidateModel candidateModel = candidatesModels.GetRepresentantModel(id, (int)Session["Year"]);

                Session["County"] = candidateModel.Candidate.County;
                Session["PoliticalPartyCode"] = candidateModel.Candidate.PoliticalPartyCode;

                // return the view
                return View(candidateModel);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }

        /// <summary>
        /// The edit is called like this: POST: /Candidate/Edit/5
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                CandidatesModels candidatesModels = new CandidatesModels();

                candidatesModels.FormCollection = collection;

                Candidate candidate = candidatesModels.Populate(collection);
                candidate.CandidateId = id;

                if (candidate.County == null)
                {
                    candidate.County = (string)Session["County"];
                }

                if (candidate.PoliticalPartyCode == null)
                {
                    candidate.PoliticalPartyCode = (string)Session["PoliticalPartyCode"];
                }

                candidatesModels.Candidate = candidate;

                string filename = _file.Upload(candidate.FirstName + " " + candidate.LastName, candidate.PoliticalPartyCode, candidate.County);
                Logger.Debug("Filename returned from file.Upload: " + filename);
                if (filename != string.Empty)
                {
                    candidatesModels.Candidate.Picture = filename;
                }
                candidatesModels.UpdateCandidate();

                return RedirectToAction("ListCandidates");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }
        
        /// <summary>
        /// The delete is called like this: GET: /Candidate/Delete/5
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Delete(int id)
        {
            try
            {
                CandidatesModels candidatesModels = new CandidatesModels();
                candidatesModels.DeleteRepresentant(id);

                return RedirectToAction("ListCandidates");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }

        /// <summary>
        /// The delete is called like this: // POST: /Candidate/Delete/5
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("ListCandidates");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }

        /// <summary>
        /// The delete election deletes the choosen election.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult DeleteElection(int id)
        {
            try
            {
                CandidatesModels model = new CandidatesModels();

                // returns the partial view after Election Data is changed
                return PartialView(
                    "ChangeElectionData",
                    model.DeleteElectionData(id, Convert.ToInt32(Request.QueryString["ElectionYear"])));
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }

        /// <summary>
        /// The change election data.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult ChangeElectionData(int id, FormCollection collection)
        {
            try
            {
                int yearId = 0;
                int county = 0;

                if (Request.QueryString["electionYear"] != null)
                {
                    yearId = Convert.ToInt32(Request.QueryString["electionYear"]);
                }

                if (collection["county" + yearId] != null)
                {
                    county = Convert.ToInt32(collection["county" + yearId]);
                }

                int position = Convert.ToInt32(collection["position" + yearId]);

                int electionYear = Convert.ToInt32(collection["electionYear" + yearId]);

                CandidatesModels model = new CandidatesModels();

                return PartialView(
                    "ChangeElectionData", model.UpdateElectionData(id, county, position, electionYear, yearId));
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }

        /// <summary>
        /// The add election data.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult AddElectionData(int id, FormCollection collection)
        {
            try
            {
                CandidatesModels model = new CandidatesModels();

                int county = Convert.ToInt32(collection["addcounty" + id]);

                int position = Convert.ToInt32(collection["addposition" + id]);

                int electionYear = Convert.ToInt32(collection["addelectionYear" + id]);

                return PartialView("ChangeElectionData", model.AddElectionData(id, county, position, electionYear));
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }

        /// <summary>
        /// The select parties.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult SelectParties(int id) 
        {
            try
            {
                CandidatesModels model = new CandidatesModels();
                ViewBag.ElectionYear = id;
                return this.View("SelectParties", model.GetAllPoliticalParties(id));
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }

        /// <summary>
        /// The select parties.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="collection">
        /// The collection.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult SelectParties(int id, FormCollection collection)
        {
            try
            {
                ElectionModels model = new ElectionModels();
                model.ConnectPartiesToElection(id, collection);

                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return this.View("Error");
            }
        }
    }
}
