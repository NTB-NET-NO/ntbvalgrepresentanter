﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HomeController.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace ElectionCandidates.Controllers
{
    using System.Web.Mvc;
    
    /// <summary>
    /// The home controller.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ViewResult"/>.
        /// </returns>
        public ViewResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View();
        }

        /// <summary>
        /// The about.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult About()
        {
            return View();
        }
    }
}