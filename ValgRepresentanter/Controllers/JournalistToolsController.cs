﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ElectionCandidates.Classes;
using ElectionCandidates.Models;
using ElectionCandidates.Models.DataModels;

namespace ElectionCandidates.Controllers
{
    public class JournalistToolsController : Controller
    {
        //
        // GET: /JournalistTools/

        public ActionResult Index()
        {
            // ElectionModels.GetElections() <- commented out because I cannot find a good way of implementing it - yet
            return View();
        }

        //
        // GET: /JournalistTools/Details/5

        public ActionResult GetSelectedStatistics()
        {
            int electionYear = Convert.ToInt32(Session["Year"]);

            if (electionYear > 0)
            {
                CandidatesModels models = new CandidatesModels();
                return View(models.GetRepresentanter(electionYear));
            }
            else
            {
                Session["ErrorMessage"] = "Du har ikke valgt valgperiode. Det må du gjøre først!";
                return View("Index");
            }
        }
        [HttpPost]
        public ActionResult GetSelectedStatistics(FormCollection collection)
        {
            int electionYear = Convert.ToInt32(Session["Year"]);

            if (electionYear > 0)
            {
                string candidates = collection["candidateId[]"];
                string gender = collection["gender"];
                string age = collection["age"];
                string profession = collection["profession"];
                string counties = collection["counties"];
                string social = collection["social"];
                string agestair = collection["agestair"];

                CandidateDataModel dataModel = new CandidateDataModel();

                // Making sure the temptable is empty
                dataModel.TruncateCandidateTempTable();

                // Populate table
                dataModel.InsertCandidateIdIntoTemptable(candidates);

                List<List<Statistic>> ListOfStatistics = new List<List<Statistic>>();
                if (gender == "on")
                {
                    List<Statistic> genderStatistics = dataModel.GetGendersFromCollection(candidates, electionYear);
                    ListOfStatistics.Add(genderStatistics);
                }
                if (age == "on")
                {
                    List<Statistic> ageStatistics = dataModel.GetAgeFromCollection(candidates, electionYear);
                    ListOfStatistics.Add(ageStatistics);
                }

                if (agestair == "on")
                {
                    List<Statistic> agestairStatistic = dataModel.GetAgeStairFromCollection(candidates, electionYear);
                    ListOfStatistics.Add(agestairStatistic);
                }

                if (profession == "on")
                {
                    List<Statistic> professionStatistics = dataModel.GetProfessionsFromCollection(candidates, electionYear);
                    ListOfStatistics.Add(professionStatistics);
                }

                if (counties == "on")
                {
                    List<Statistic> countiesStatistics = dataModel.GetCountiesFromCollection(candidates, electionYear);
                    ListOfStatistics.Add(countiesStatistics);
                }

                if (social == "on")
                {
                    List<Statistic> socialmediaStatistics = dataModel.GetSocialMediasFromCollection(candidates, electionYear);
                    ListOfStatistics.Add(socialmediaStatistics);
                }

                dataModel.TruncateCandidateTempTable();
                return View("ShowStatistics", ListOfStatistics);
            } else {
                Session["ErrorMessage"] = "Du har ikke valgt valgperiode. Det må du gjøre først!";
                return View("Index");
            }
        }

        //
        // GET: /JournalistTools/Create

        public ActionResult GetSelectedStatisticsFromParliament()
        {
            return View();
        } 

        //
        // POST: /JournalistTools/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /JournalistTools/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /JournalistTools/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /JournalistTools/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /JournalistTools/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
